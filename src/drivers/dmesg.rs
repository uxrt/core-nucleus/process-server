/*
 * Copyright (c) 2024-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///Process manager log driver (similar to /dev/kmsg on Linux)
use alloc::sync::Arc;
use alloc::vec::Vec;
use core::str;

use typed_path::{
	UnixPath,
	UnixPathBuf,
};

use sel4_thread::WrappedThread;

use crate::add_device_ocb_slabs;

use crate::logger;

use crate::task::get_task_tree;

use crate::vfs::new_resmgr_port;

use uxrt_vfs_resmgr::{
	CommonOCB,
	DirectoryIOHandler,
	DispatchContext,
	DispatchContextFactory,
	OpenControlBlock,
	PrimitiveIOHandler,
	ResMgrContextExtra,
	RPCConnectHandler,
	RPCIOHandler,
	device_rpc_connect_handler,
	device_rpc_io_handler,
	device_primitive_io_handler,
};

use uxrt_vfs_resmgr::inner::{
	DeviceOCB,
	DeviceMessageHandler,
	DeviceInnerMsgHandler,
	DeviceResMgrContext,
	DeviceResMgrPort,
};

use uxrt_vfs_rpc::{
	O_TRANSIENT,
	S_IFMT,
	S_IFCHR,
	VFSServerOpenResult,
	mode_t,
	open_how_server,
};

use uxrt_transport_layer::{
	FileDescriptor,
	IOError,
	Offset,
	OffsetType,
	SRV_ERR_IO,
	ServerStatus,
};

const DMESG_BUF_SIZE: usize = 8192;

///OCB for /dev/dmesg handler
struct DmesgOCB {
	common_ocb: CommonOCB,
}

impl DmesgOCB {
	///Creates a new `DmesgOCB`.
	pub fn new() -> DeviceOCB<Self> {
		DeviceOCB::new(Self {
			common_ocb: CommonOCB::new(),
		})
	}
}

impl OpenControlBlock for DmesgOCB {
	fn get_common<'a>(&'a self) -> &'a CommonOCB {
		&self.common_ocb
	}
	fn get_common_mut<'a>(&'a mut self) -> &'a mut CommonOCB {
		&mut self.common_ocb
	}
}

///Context state for /dev/dmesg handler
struct DmesgContextExtra {
	buf: Vec<u8>,
	printed_msg_len: usize,
	msg_len: usize,
}

impl ResMgrContextExtra for DmesgContextExtra {
	fn new() -> Self {
		let mut ret = Self {
			buf: Vec::with_capacity(DMESG_BUF_SIZE),
			printed_msg_len: 0,
			msg_len: 0,
		};
		unsafe { ret.buf.set_len(DMESG_BUF_SIZE) };
		ret
	}
}

///Central handler for log messages
pub struct DmesgResMgr {
}

impl DmesgResMgr {
	///Creates a new `DmesgResMgr`.
	fn new() -> Arc<DeviceMessageHandler<DmesgOCB, Self, DmesgContextExtra>> {
		let path = UnixPathBuf::from("dev/dmesg");
		DeviceMessageHandler::new(path, Self {
		}, 0o755, S_IFCHR as mode_t | 0o666).expect("cannot create device message handler for /dev/dmesg")
	}
	///Writes a line to the log.
	fn write_line(&self, line: &[u8]){
		let string = match str::from_utf8(line) {
			Ok(s) => s,
			Err(err) => {
				println!("warning: invalid UTF-8 in log message; truncated:");
				str::from_utf8(&line[..err.valid_up_to()]).unwrap()
			},
		};
		print!("{}", string);
	}
}

trait DmesgMsgHandler = DeviceInnerMsgHandler<DmesgOCB, DmesgContextExtra>;
type DmesgResMgrContext<H> = DeviceResMgrContext<DmesgOCB, H, DmesgContextExtra>;
type DmesgOuterOCB = DeviceOCB<DmesgOCB>;


device_rpc_connect_handler!(DmesgResMgr, DmesgOCB, DmesgContextExtra, {
	fn handle_openat2<H: DmesgMsgHandler>(&self, context: &DmesgResMgrContext<H>, _path: &mut [u8], _path_size: usize, how: &open_how_server, _res_offset: usize) -> Result<(VFSServerOpenResult, usize), ServerStatus>{
		//info!("DmesgResMgr::handle_openat2");
		if how.flags & O_TRANSIENT as u64 == 0 {
			context.bind_ocb(DmesgOCB::new()).or_else(|_| { Err(SRV_ERR_IO) })?;
		}
		Ok(VFSServerOpenResult::inode(0))
	}
});

device_rpc_io_handler!(DmesgResMgr, DmesgOCB, DmesgContextExtra, {
});

device_primitive_io_handler!(DmesgResMgr, DmesgOCB, DmesgContextExtra, {
	fn handle_wpreadv_outer<H: DmesgMsgHandler>(&self, context: &DmesgResMgrContext<H>, _ocb: &mut DmesgOuterOCB, size: usize, offset: i64, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		panic!("DmesgResMgr: TODO: implement handle_wpreadv_outer; add a backlog buffer and allow reading it");
	}
	fn handle_wpwritev_outer<H: DmesgMsgHandler>(&self, context: &DmesgResMgrContext<H>, _ocb: &mut DmesgOuterOCB, size: usize, offset: i64, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		if offset != 0 || whence.base() != OffsetType::Current {
			return Err(IOError::InvalidArgument);
		}
		if size == 0 {
			return Ok((0, 0));
		}
		let mut extra = context.get_extra().borrow_mut();
		let message_fd = context.get_message_fd().unwrap();

		let msg_len = extra.msg_len;
		let mut out_buf = &mut extra.buf[msg_len..];

		message_fd.read(&mut out_buf)?;

		let max = extra.buf.len();
		extra.msg_len += size;
		if extra.msg_len > max {
			extra.buf[max - 1] = '\n' as u8;
			extra.msg_len = max;
		}
		//info!("msg len: {} {}", extra.printed_msg_len, extra.msg_len);

		for i in extra.printed_msg_len..extra.msg_len {
			if extra.buf[i] == '\n' as u8 {
				let start = extra.printed_msg_len;
				extra.printed_msg_len = i + 1;
				self.write_line(&extra.buf[start..extra.printed_msg_len]);
			}
		}

		if extra.printed_msg_len == extra.msg_len {
			extra.printed_msg_len = 0;
			extra.msg_len = 0;
		}

		Ok((size, 0))
	}
	fn handle_clunk<H: DmesgMsgHandler>(&self, _context: &DmesgResMgrContext<H>, _ocb: &mut DmesgOuterOCB) {
	}
});

///Main function of the /dev/dmesg handler.
fn dmesg_main(port: Arc<DeviceResMgrPort<DmesgOCB, DmesgResMgr, DmesgContextExtra>>){
	logger::use_dev_dmesg_thread(Some(false));
	let mut resmgr_context = port.new_context().expect("could not create context for test resource manager");

	loop {
		resmgr_context.block().expect("could not block for test resource manager message");
		let _ = resmgr_context.handle_message();
	}
}

///Starts the /dev/dmesg handler.
pub fn start_dmesg(){
	info!("starting /dev/dmesg handler");
	add_dmesg_slabs();
	let port = new_resmgr_port(DmesgResMgr::new(), 0, "/", &[&UnixPath::new("dev/dmesg"), &UnixPath::new("dev")]).expect("could not create dmesg resource manager port");
	let server_thread = get_task_tree().new_root_thread(None).expect("could not allocate dmesg resource manager thread");
	server_thread.write().set_name("dmesg");
	server_thread.write().run(move ||{
		dmesg_main(port);
		None
	}).expect("failed to run dmesg resource manager thread");
	logger::init_dmesg();
}

pub fn add_dmesg_slabs(){
	add_device_ocb_slabs!(DmesgOCB, 512, 4, 4, 2, "/dev/dmesg OCB");
}
