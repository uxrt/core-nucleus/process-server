/*
 * Copyright (c) 2018-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 */

#[cfg(feature = "test_alloc")]
pub mod alloc;

#[cfg(feature = "test_task")]
pub mod task;

#[cfg(feature = "test_vfs")]
pub mod vfs;

#[cfg(feature = "test_vfs")]
pub mod drivers;

use ::alloc::sync::Arc;
use ::alloc::vec::Vec;
use usync::RwLock;
use crate::task::get_task_tree;
use crate::task::thread::{
	ExitEndpoint,
	SysThread,
};
use sel4_thread::WrappedThread;

const NUM_TEST_THREADS: usize = 7;
const MAX_CORE: usize = 3;
#[cfg(any(feature = "test_task", feature = "test_vfs"))]
pub fn create_local_test_threads() -> Vec<Arc<RwLock<SysThread>>>{
	info!("creating local test threads");
	let task_tree = get_task_tree();
	info!("creating test thread 0");
	let local_thread_0 = task_tree.new_root_thread(ExitEndpoint::private()).expect("could not allocate local thread");
	info!("creating test thread 1");
	let local_thread_1 = task_tree.new_root_thread(ExitEndpoint::private()).expect("could not allocate local thread");
	//local_thread_0.write().set_core(1).expect("couldn't change core of test thread 1");
	local_thread_0.write().set_name("test_thread_0");
	local_thread_1.write().set_core(2).expect("couldn't change core of test thread 1");
	local_thread_1.write().set_name("test_thread_1");
	let local_thread_2 = task_tree.new_root_thread(ExitEndpoint::private()).expect("could not allocate local thread");
	local_thread_2.write().set_name("test_thread_2");

	let mut ret = Vec::new();
	ret.push(local_thread_0);
	ret.push(local_thread_1);
	ret.push(local_thread_2);

	let mut core = 0;
	while ret.len() <= NUM_TEST_THREADS {
		let thread = task_tree.new_root_thread(ExitEndpoint::private()).expect("could not allocate local thread");
		let mut t = thread.write();
		t.set_name(format!("test_thread_{}", ret.len()).as_str());
		t.set_core(core).expect("couldn't change core of test thread");
		drop(t);
		core += 1;
		if core > MAX_CORE {
			core = 0;
		}
		ret.push(thread);
	}

	ret
}

#[cfg(any(feature = "test_task", feature = "test_vfs"))]
pub fn deallocate_local_threads(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	while threads.len() > 0 {
		let thread = threads.pop().unwrap();
		let id = thread.read().tid;
		info!("deallocating test thread {}", id);
		get_task_tree().delete_root_thread(id).expect("cannot delete root server thread");
	}
}

/* vim: set softtabstop=8 tabstop=8 noexpandtab: */
