/*
 * Copyright (c) 2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * Tests for the null driver
 */

use uxrt_procfs_client::{
	PORT_CHANNEL_GET_RECV_TOTAL_SIZE,
	PORT_CHANNEL_GET_REPLY_SIZE,
	PORT_UNLINK_ON_NO_SERVERS,
};

use uxrt_procfs_client::port::{
	PortFSClient,
	PortType,
};

use crate::tests::vfs::allocate_fds;
use crate::tests::vfs::rpc::client::{
	test_chmod_single,
	test_chown_single,
	test_stat_single,
};

use uxrt_transport_layer::{
	AccessMode,
	FileDescriptor,
	FileDescriptorRef,
	SRV_ERR_NOSPC,
};

use uxrt_vfs_rpc_client::VFSRPCClient;
use uxrt_vfs_rpc::{
	S_IFMSG,
	O_RDWR,
};

use crate::vfs::transport::get_fd;
use crate::vfs::rpc::get_default_rpc_client;

fn test_portfs_rpc_single(rpc: &VFSRPCClient<FileDescriptorRef>, path: &str, orig_mode: u32) {
	test_stat_single(rpc, path, orig_mode);
	test_chown_single(rpc, path, orig_mode);
	test_chmod_single(rpc, path, orig_mode);
}

fn test_portfs_rpc_port(rpc: &VFSRPCClient<FileDescriptorRef>, portfs_client: &PortFSClient, port_id: i32, server_fd: i32, client_fd: i32){
	let server_path = format!("/proc/fscontext/self/ports/server/{}", port_id);
	test_portfs_rpc_single(&rpc, &server_path, S_IFMSG | 0o600);
	if client_fd >= 0 {
		let client_path = format!("/proc/fscontext/self/ports/client/{}", port_id);
		test_portfs_rpc_single(&rpc, &client_path, S_IFMSG | 0o600);
	}
	portfs_client.unlink(port_id).expect("could not unlink channel port");
	if client_fd >= 0 {
		rpc.close(client_fd).expect("could not close channel port client FD");
	}
	rpc.close(server_fd).expect("could not close channel port server FD");
}

pub fn test_portfs_rpc(){
	let portfs_client = PortFSClient::new(None, true);
	let vfs_client = get_default_rpc_client();

	test_stat_single(&vfs_client, "/proc/fscontext/self/ports/create", S_IFMSG | 0o600);

	/*let (channel_port_id, server_channel_fd, client_channel_fd, server_message_fd, message_port_id) = allocate_fds(PortType::DirectChannel,
		(PORT_CHANNEL_GET_REPLY_SIZE | PORT_CHANNEL_GET_RECV_TOTAL_SIZE) as u64,
		AccessMode::ReadWrite);
	test_portfs_rpc_port(&vfs_client, &portfs_client, channel_port_id, server_channel_fd, client_channel_fd);
	test_portfs_rpc_port(&vfs_client, &portfs_client, message_port_id, server_message_fd, -1);*/

	/*let (notification_port_id, server_notification_fd, client_notification_fd, _, _) = allocate_fds(PortType::Notification,
		0,
		AccessMode::ReadWrite);
	test_portfs_rpc_port(&vfs_client, &portfs_client, notification_port_id, server_notification_fd, client_notification_fd);*/

}
/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
