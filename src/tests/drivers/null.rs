/*
 * Copyright (c) 2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * Tests for the null driver
 */

use sel4::{
	RecvLongMsgBuffer,
	SendLongMsgBuffer,
};
use uxrt_transport_layer::{
	FileDescriptor,
	FileDescriptorRef,
	SRV_ERR_NOSPC,
};
use uxrt_vfs_rpc::O_RDWR;

use crate::tests::vfs::transport::channel::extended::{
	allocate_extended_receive_buf_no_fault,
	deallocate_extended_test_buffers,
};

use crate::vfs::transport::get_fd;
use crate::vfs::rpc::get_default_rpc_client;

const ZERO_TEST_BUF_SIZE: usize = 10485760;

fn assert_buf(buf: &[u8], val: u8){
	for i in 0..buf.len(){
		assert!(buf[i] == val, "invalid value {} in test buffer at {}", buf[i], i);
	}
}

fn open_null(path: &str) -> FileDescriptorRef {
	let rpc = get_default_rpc_client();
	get_fd(rpc.open(path.as_bytes(), O_RDWR as u64).expect("could not open null device"))
}

fn close_null(fd: FileDescriptorRef){
	get_default_rpc_client().close(fd.get_id()).expect("could not close null device");
}

fn check_zero_read(fd: FileDescriptorRef, buf_addr: *mut u8, read_size: usize, writeread: bool){
	let mut read_buf_info = unsafe { [
		RecvLongMsgBuffer::new_raw(buf_addr, usize::min(read_size, ZERO_TEST_BUF_SIZE)),
	] };

	let write_buf = [0u8; 1024];
	let write_buf_info = [SendLongMsgBuffer::new(&write_buf)];

	read_buf_info[0].fill(0xff);
	let size = if writeread {
		let (w_size, r_size) = fd.writereadv(&write_buf_info, &mut read_buf_info).expect("failed to read from /dev/zero");
		assert!(w_size == write_buf.len(), "writeread returned invalid write size {}", w_size);
		r_size
	}else{
		fd.readv(&mut read_buf_info).expect("failed to read from /dev/zero")
	};
	assert!(size == read_buf_info[0].len, "/dev/zero read returned invalid size {}", size);
	assert_buf(&read_buf_info[0], 0);
}

fn check_write(fd: FileDescriptorRef){
	let size = fd.write(&[1u8; 1024]).expect("failed to write to null device");
	assert!(size == 1024, "null device write returned invalid size {}", size);
}

pub fn test_full(read_buf_addr: *mut u8){
	let write_buf = [1u8; 1024];
	info!("opening /dev/full");
	let fd = open_null("/dev/full");

	info!("writing to /dev/full");
	let err = fd.write(&write_buf).expect_err("write to /dev/full succeeded");
	assert!(err.to_errno() == SRV_ERR_NOSPC, "/dev/full returned unexpected error {:?}", err);

	info!("reading into full buffer from /dev/full");
	check_zero_read(fd, read_buf_addr, ZERO_TEST_BUF_SIZE, false);

	info!("reading into partial buffer from /dev/full");
	check_zero_read(fd, read_buf_addr, 1024, false);

	info!("closing /dev/full");
	close_null(fd);

}

pub fn test_null(){
	let mut read_buf = [1u8; 1024];

	info!("opening /dev/null");
	let fd = open_null("/dev/null");

	info!("reading from /dev/null");
	let size = fd.read(&mut read_buf).expect("failed to read from /dev/null");
	assert!(size == 0, "/dev/null read returned non-zero size {}", size);
	assert_buf(&read_buf, 1);

	info!("writing to /dev/null");
	check_write(fd);

	info!("closing /dev/null");
	close_null(fd);
}

pub fn test_zero(read_buf_addr: *mut u8){
	info!("opening /dev/zero");
	let fd = open_null("/dev/zero");

	info!("writing to /dev/zero");
	check_write(fd);

	info!("reading into full buffer from /dev/zero");
	check_zero_read(fd, read_buf_addr, ZERO_TEST_BUF_SIZE, false);

	info!("reading into partial buffer from /dev/zero");
	check_zero_read(fd, read_buf_addr, 1024, false);

	info!("reading into full buffer from /dev/zero with writeread");
	check_zero_read(fd, read_buf_addr, ZERO_TEST_BUF_SIZE, true);

	info!("reading into partial buffer from /dev/zero with writeread");
	check_zero_read(fd, read_buf_addr, 1024, true);


	info!("closing /dev/zero");
	close_null(fd);

}

pub fn test_null_devs(){
	let zero_buf_addr = allocate_extended_receive_buf_no_fault(ZERO_TEST_BUF_SIZE) as *mut u8;
	test_full(zero_buf_addr);
	test_null();
	test_zero(zero_buf_addr);
	deallocate_extended_test_buffers();
}
/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
