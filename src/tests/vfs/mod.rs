/*
 * Copyright (c) 2022-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module includes core code for testing the VFS and transport layer.
 */

pub mod transport;
pub mod rpc;

use super::drivers::run_driver_tests;
use core::sync::atomic::{AtomicBool, Ordering};
use self::transport::run_transport_layer_tests;
use self::rpc::run_vfs_rpc_tests;
pub use self::transport::channel::handle_extended_test_page_fault;
use alloc::sync::Arc;

use crate::task::thread::SysThread;

use usync::RwLock;

use uxrt_procfs_client::PORT_OPEN_AFTER_CREATE;
use uxrt_procfs_client::port::{
	PortFSClient,
	PortType,
};

use uxrt_vfs_rpc::flags_from_access;

use uxrt_transport_layer::AccessMode;

use crate::{
	dump_heap,
	dump_utspace,
};

use alloc::vec::Vec;

pub static ENABLE_SHARED: AtomicBool = AtomicBool::new(true);

pub fn allocate_fds(port_type: PortType, mut port_flags: u64, access: AccessMode) -> (i32, i32, i32, i32, i32) {
	port_flags |= PORT_OPEN_AFTER_CREATE as u64;
	let portfs_client = PortFSClient::new(None, true);
	let (message_port_id, server_message_fd) = if port_type == PortType::DirectChannel {
		info!("allocate_fds: creating and attaching message port");
		let (p, f) = portfs_client.create(PortType::MessageDescriptor, PORT_OPEN_AFTER_CREATE as u64).expect("cannot create server message descriptor");
		(p, f)
	}else{
		(-1, -1)
	};

	info!("allocate_fds: creating and attaching channel port");
	let (channel_port_id, server_channel_fd) = portfs_client.create(port_type, port_flags).expect("cannot create primary server port descriptor");
	info!("allocate_fds: attaching client side of channel port");
	let client_id = portfs_client.open_client(channel_port_id, flags_from_access(access)).expect("cannot attach client port descriptor").get_id();

	info!("Heap status after FD allocation:");
	dump_heap();
	info!("UTSpace status after FD allocation:");
	dump_utspace();
	(channel_port_id, server_channel_fd, client_id, server_message_fd, message_port_id)
}

//const NUM_VFS_TESTS: usize = usize::MAX;
const NUM_VFS_TESTS: usize = 50;
//const NUM_VFS_TESTS: usize = 1;

pub fn test_vfs(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	for i in 0..NUM_VFS_TESTS {
		info!("running VFS tests: {}", i);
		info!("Heap status before VFS tests:");
		dump_heap();
		info!("UTSpace status before VFS tests:");
		dump_utspace();

		run_vfs_rpc_tests(threads);
		run_driver_tests(threads);
		run_transport_layer_tests(threads);

		info!("Heap status after VFS tests:");
		dump_heap();
		info!("UTSpace status after VFS tests:");
		dump_utspace();
		info!("Ports after VFS tests:");
		crate::vfs::rpc::dump_ports();
	}
}
/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
