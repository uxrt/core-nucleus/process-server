/*
 * Copyright (c) 2024-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module tests the resource manager side of the VFS RPC layer.
 */

use alloc::string::String;
use alloc::sync::Arc;
use alloc::vec::Vec;

use core::sync::atomic::{
	AtomicI32,
	Ordering,
};

use typed_path::UnixPath;

use sel4_thread::WrappedThread;

use crate::task::thread::SysThread;
use crate::task::thread_pool::new_resmgr_thread_pool;
use crate::task::get_task_tree;

use crate::tests::vfs::rpc::client::test_dir;

use crate::vfs::new_resmgr_port;

use crate::vfs::rpc::{
	get_default_rpc_client,
	get_root_default_fs_context,
};

use usync::RwLock;

use uxrt_vfs_resmgr::inner::{
	TreeContextExtra,
	TreeInnerMsgHandler,
	TreeMessageHandler,
	TreeNodeNonLinkExtra,
	TreeOCB,
	TreeResMgrContext,
	TreeResMgrPort,
};

use uxrt_vfs_resmgr::{
	CommonOCB,
	DirectoryIOHandler,
	DispatchContext,
	DispatchContextFactory,
	OpenControlBlock,
	PrimitiveIOHandler,
	ResMgrContextExtra,
	RPCConnectHandler,
	RPCIOHandler,
	tree_rpc_connect_handler,
	tree_rpc_io_handler,
	tree_primitive_io_handler,
};

use uxrt_vfs_rpc::{
	AT_FDCWD,
	AT_FDINVALID,
	O_DIRECTORY,
	O_NOFOLLOW,
	O_RDONLY,
	O_RDWR,
	O_USEDEST,
	RESOLVE_NO_SYMLINKS,
	S_IFDIR,
	S_IFREG,
	VFSServerOpenResult,
	mode_t,
	open_how,
	open_how_server,
};

use uxrt_transport_layer::{
	FileDescriptor,
	IOError,
	Offset,
	OffsetType,
	SRV_ERR_INVAL,
	SRV_ERR_IO,
	SRV_ERR_LOOP,
	SRV_ERR_NOSPC,
	ServerStatus,
	get_fd,
};

use super::super::transport::channel::basic::{
	test_client_basic,
	test_client_basic_copy,
	test_server_basic_copy_read,
	test_server_basic_copy_write,
};

pub struct TestOCB {
	common_ocb: CommonOCB,
}

impl TestOCB {
	pub fn new() -> Self {
		Self {
			common_ocb: CommonOCB::new(),
		}
	}
}

impl OpenControlBlock for TestOCB {
	fn get_common<'a>(&'a self) -> &'a CommonOCB {
		&self.common_ocb
	}
	fn get_common_mut<'a>(&'a mut self) -> &'a mut CommonOCB {
		&mut self.common_ocb
	}
}

impl Drop for TestOCB {
	fn drop(&mut self) {
		info!("TestOCB::drop");
	}
}

pub struct TestContextExtra {
}

impl ResMgrContextExtra for TestContextExtra {
	fn new() -> Self {
		Self {}
	}
}

pub struct TestNodeExtra {
}

impl TreeNodeNonLinkExtra for TestNodeExtra {
}

///Runs the basic transport layer test within a resource manager instead of a direct channel
struct TestResMgr {
}

impl TestResMgr {
	fn new(patterns: &[(&str, usize, usize)]) -> Arc<TreeMessageHandler<TestOCB, Self, TestContextExtra, TestNodeExtra>> {
		let handler = TreeMessageHandler::new(Self {}, TestNodeExtra {}).expect("cannot create null device message handler");
		handler.add_node(UnixPath::new("test"), (S_IFREG | 0o666) as mode_t, 0, 0, TestNodeExtra { }).expect("cannot add test entry to test message handler");
		handler.add_node_symlink(UnixPath::new("test_rel_symlink"), Vec::from("test".as_bytes())).expect("cannot add test entry to test message handler");

		handler.add_node_symlink(UnixPath::new("full"), Vec::from("/dev/full".as_bytes())).expect("cannot add test entry to test message handler");
		handler.add_node_firmlink(UnixPath::new("full_firm"), Vec::from("/dev/full".as_bytes())).expect("cannot add test entry to test message handler");
		handler.add_node_symlink(UnixPath::new("dev"), Vec::from("/dev".as_bytes())).expect("cannot add test entry to test message handler");

		handler.add_node(UnixPath::new("quux"), (S_IFDIR | 0o666) as mode_t, 0, 0, TestNodeExtra { }).expect("cannot add test entry to test message handler");
		handler.add_node(UnixPath::new("quux/qux"), (S_IFDIR | 0o666) as mode_t, 0, 0, TestNodeExtra { }).expect("cannot add test entry to test message handler");
		handler.add_node(UnixPath::new("quux/qux/baz"), (S_IFDIR | 0o666) as mode_t, 0, 0, TestNodeExtra { }).expect("cannot add test entry to test message handler");
		handler.add_node(UnixPath::new("quux/qux/baz/bar"), (S_IFDIR | 0o666) as mode_t, 0, 0, TestNodeExtra { }).expect("cannot add test entry to test message handler");
		handler.add_node(UnixPath::new("quux/qux/baz/bar/foo"), (S_IFDIR | 0o666) as mode_t, 0, 0, TestNodeExtra { }).expect("cannot add test entry to test message handler");

		handler.add_node_symlink(UnixPath::new("quux/qux/baz/bar/foo/full0"), Vec::from("../../../../../../dev/full".as_bytes())).expect("cannot add test entry to test message handler");
		handler.add_node_symlink(UnixPath::new("quux/qux/baz/bar/foo/full1"), Vec::from("......../dev/full".as_bytes())).expect("cannot add test entry to test message handler");
		handler.add_node_symlink(UnixPath::new("quux/qux/baz/bar/foo/full2"), Vec::from(".../../../../../dev/full".as_bytes())).expect("cannot add test entry to test message handler");

		handler.add_node_symlink(UnixPath::new("quux/qux/baz/full0"), Vec::from("..../full".as_bytes())).expect("cannot add test entry to test message handler");
		handler.add_node_symlink(UnixPath::new("quux/qux/baz/full1"), Vec::from("../../../full".as_bytes())).expect("cannot add test entry to test message handler");
		handler.add_node_symlink(UnixPath::new("quux/qux/baz/full2"), Vec::from(".../../full".as_bytes())).expect("cannot add test entry to test message handler");



		for (base, start, end) in patterns {
			for i in *start..*end {
				let name = format!("{}{:02}", base, i);
				handler.add_node(UnixPath::new(&name), (S_IFREG | 0o666) as mode_t, 0, 0, TestNodeExtra {}).expect("cannot add extra test entry to test message handler");
			}
		}
		handler
	}
}


trait TestInnerMsgHandler = TreeInnerMsgHandler<TestOCB, TestContextExtra, TestNodeExtra>;
type TestResMgrContext<H> = TreeResMgrContext<TestOCB, H, TestContextExtra, TestNodeExtra>;
type TestOuterOCB = TreeOCB<TestOCB, TestNodeExtra>;


tree_rpc_connect_handler!(TestResMgr, TestOCB, TestContextExtra, TestNodeExtra, {
	fn handle_openat2<H: TestInnerMsgHandler>(&self, context: &TestResMgrContext<H>, path: &mut [u8], path_size: usize, _how: &open_how_server, _res_offset: usize) -> Result<(VFSServerOpenResult, usize), ServerStatus>{
		let path_utf8 = core::str::from_utf8(&path[..path_size]).expect("non-UTF-8 sequence in path");
		info!("TestResMgr::handle_openat2_server: {}", path_utf8);
		let mut extra = context.get_extra().borrow_mut();
		let ocb = extra.new_ocb(TestOCB::new()).or(Err(SRV_ERR_IO))?;

		context.bind_ocb(ocb).expect("could not bind test OCB");
		Ok(VFSServerOpenResult::inode(0))
	}
});

tree_rpc_io_handler!(TestResMgr, TestOCB, TestContextExtra, TestNodeExtra, {
});

tree_primitive_io_handler!(TestResMgr, TestOCB, TestContextExtra, TestNodeExtra, {
	fn handle_wpreadv_outer<H: TestInnerMsgHandler>(&self, context: &TestResMgrContext<H>, _ocb: &mut TestOuterOCB, size: usize, offset: i64, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		info!("TestResMgr::handle_wpreadv_outer: {} {} {:?}", size, offset, whence);
		let message_fd = context.get_message_fd().ok_or(IOError::ServerError(SRV_ERR_INVAL))?;
		let (sw_buf, offset, status) = test_server_basic_copy_read(&[], size, offset as usize, whence);
		if status >= 0 {
			message_fd.wpwrite(&sw_buf, 0, OffsetType::Start)?;
			Ok((0, offset as Offset))
		}else{
			Err(IOError::ServerError(-status))
		}
	}
	fn handle_wpwritev_outer<H: TestInnerMsgHandler>(&self, context: &TestResMgrContext<H>, _ocb: &mut TestOuterOCB, size: usize, offset: i64, whence: OffsetType) -> Result<(usize, Offset), IOError> {
		info!("TestResMgr::handle_wpwritev_outer: {} {} {:?}", size, offset, whence);
		let message_fd = context.get_message_fd().ok_or(IOError::ServerError(SRV_ERR_INVAL))?;
		let mut sr_buf = Vec::with_capacity(size);
		unsafe { sr_buf.set_len(size) };
		message_fd.read(&mut sr_buf)?;
		let (_, offset, status) = test_server_basic_copy_write(&sr_buf, size, offset as usize, whence);
		if status >= 0 {
			Ok((status as usize, offset as Offset))
		}else{
			Err(IOError::ServerError(-status))
		}
	}
	fn handle_clunk<H: TestInnerMsgHandler>(&self, _context: &TestResMgrContext<H>, _ocb: &mut TestOuterOCB) {
		info!("TestResMgr::handle_clunk");
	}
});

fn test_resmgr_st_main(port: Arc<TreeResMgrPort<TestOCB, TestResMgr, TestContextExtra, TestNodeExtra>>){
	let mut resmgr_context = port.new_context().expect("could not create context for test resource manager");

	loop {
		resmgr_context.block().expect("could not block for test resource manager message");
		let _ = resmgr_context.handle_message();
	}
}

fn resmgr_test_client(path: &str, iterations: usize, do_global: bool){
	info!("resmgr_test_client: using path {:?}", path);
	info!("opening absolute path with unspecified FD ID");
	let rpc = get_default_rpc_client();
	let mut abs_fd = rpc.open(path.as_bytes(), O_RDWR as u64).expect("could not open test resource manager connection with absolute path");
	resmgr_test_client_inner_0(abs_fd, iterations);

	if do_global {
		info!("opening absolute path with FD 1024");
		let how = open_how {
			flags: (O_RDWR | O_USEDEST) as u64,
			mode: 1024 << 32,
			resolve: 0,
		};
		abs_fd = rpc.openat2(AT_FDINVALID, path.as_bytes(), &how).expect("could not open test resource manager connection with absolute path and fixed FD");
		resmgr_test_client_inner_0(abs_fd, iterations);
	}

	info!("opening relative path with explicit dirfd");
	let full_path = UnixPath::new(path.as_bytes());
	assert!(full_path.is_absolute(), "path not absolute");
	let dir_path = full_path.parent().expect("path does not contain a directory name");
	info!("directory path: {}", dir_path);
	let rel_path = full_path.file_name().expect("path does not contain a file name");
	let mut dir_fd = rpc.open(dir_path.as_bytes(), (O_RDONLY | O_DIRECTORY) as u64).expect("could not open test resource manager directory");
	let mut rel_fd = rpc.openat(dir_fd, rel_path, O_RDWR as u64).expect("could not open test resource manager connection with relative path");
	resmgr_test_client_inner_0(rel_fd, iterations);

	if do_global {
		info!("opening relative path with CWD obtained by chdir");
		rpc.chdir(dir_path.as_bytes()).expect("could not chdir to directory path");
		rel_fd = rpc.open(rel_path, O_RDWR as u64).expect("could not open test resource manager connection with CWD obtained by chdir");
		resmgr_test_client_inner_0(rel_fd, iterations);

		info!("chdir()ing to /dev");
		rpc.chdir("/dev".as_bytes()).expect("could not chdir to /dev");

		info!("opening relative path with CWD obtained by fchdir");
		rpc.fchdir(dir_fd).expect("could not fchdir to directory FD");

		rel_fd = rpc.open(rel_path, O_RDWR as u64).expect("could not open test resource manager connection with CWD obtained by fchdir");
		resmgr_test_client_inner_0(rel_fd, iterations);
	}
	rpc.close(dir_fd).expect("failed to close directory file descriptor");

	info!("opening relative path with explicit dirfd opened without O_DIRECTORY");
	dir_fd = rpc.open(dir_path.as_bytes(), O_RDONLY as u64).expect("could not open test resource manager directory");
	rel_fd = rpc.openat(dir_fd, rel_path, O_RDWR as u64).expect("could not open test resource manager connection with relative path");
	resmgr_test_client_inner_0(rel_fd, iterations);
	rpc.close(dir_fd).expect("failed to close directory file descriptor");
}

fn resmgr_test_client_inner_0(orig_fd: i32, iterations: usize){
	let rpc = get_default_rpc_client();

	resmgr_test_client_inner_1(orig_fd, iterations);

	let dup_fd = rpc.dup3(orig_fd, AT_FDINVALID, 0).expect("failed to duplicate FD");

	resmgr_test_client_inner_1(dup_fd, iterations);

	info!("closing resource manager connections");
	rpc.close(orig_fd).expect("could not close test resource manager connection");
	resmgr_test_client_inner_1(dup_fd, iterations);

	rpc.close(dup_fd).expect("could not close test resource manager connection");
}

fn resmgr_test_client_inner_1(fd: i32, iterations: usize) {
	info!("opened resource manager connection: {}", fd);
	for _ in 0..iterations {
		test_client_basic(fd);
		test_client_basic_copy(fd);
	}
}

fn new_test_resmgr_port(path: &str, patterns: &[(&str, usize, usize)]) -> Arc<TreeResMgrPort<TestOCB, TestResMgr, TestContextExtra, TestNodeExtra>> {
	new_resmgr_port(TestResMgr::new(patterns), 0, path, &[]).expect("cannot create test resource manager port")
}

const MT_CLIENT_TEST_ITERATIONS: usize = 10;
static TEST_RESMGR_PORT: AtomicI32 = AtomicI32::new(-1);


fn start_test_resmgrs() -> i32{
	if TEST_RESMGR_PORT.load(Ordering::Relaxed) == -1 {
		info!("starting single-threaded test resource manager");
		let st_port = new_test_resmgr_port("/test_st", &[("baz", 0, 64), ("qux", 0, 64)]);

		let st_port_id = st_port.get_port_id();
		info!("starting server thread");
		let server_thread = get_task_tree().new_root_thread(None).expect("could not allocate test resource manager thread");
		server_thread.write().set_name("test_resmgr_st");
		server_thread.write().run(move ||{
			test_resmgr_st_main(st_port);
			None
		}).expect("failed to run test resource manager thread");
		TEST_RESMGR_PORT.store(st_port_id, Ordering::Relaxed);

		info!("starting first multithreaded test resource manager");
		let mt_port_0 = new_test_resmgr_port("/test_mt",
						     &[("foo", 0, 64),
						     ("bar", 0, 32)]);
		let pool_0 = new_resmgr_thread_pool(&mt_port_0, 3, 5, 10, 2, "test_resmgr_mt_0").expect("could not create thread pool for test resource manager");
		pool_0.start().expect("could not create thread pool for test resource manager");

		let mt_port_1 = new_test_resmgr_port("/test_mt",
						     &[("foo", 0, 32),
						     ("bar", 0, 64)]);
		let pool_1 = new_resmgr_thread_pool(&mt_port_1, 3, 5, 10, 2, "test_resmgr_mt_1").expect("could not create thread pool for test resource manager");
		pool_1.start().expect("could not create thread pool for test resource manager");

		let mt_port_2 = new_test_resmgr_port("/test_mt",
						     &[("foo", 0, 2),
						     ("bar", 64, 72)]);
		let pool_2 = new_resmgr_thread_pool(&mt_port_2, 3, 5, 10, 2, "test_resmgr_mt_1").expect("could not create thread pool for test resource manager");
		pool_2.start().expect("could not create thread pool for test resource manager");

		let fs_context = get_root_default_fs_context();
		fs_context.mount(-1, UnixPath::new("/:hide"), UnixPath::new(&"/test_mt")).expect("failed to mount hide path");
		let test_st_src = format!("/:mount:{}:{}", fs_context.get_id(), st_port_id);
		fs_context.mount(-1, UnixPath::new(&test_st_src), UnixPath::new(&"/test_mt")).expect("failed to mount single-threaded test resource manager");
		st_port_id
	}else{
		TEST_RESMGR_PORT.load(Ordering::Relaxed)
	}
}

fn generate_name_list<'a>(patterns: &[(&str, usize, usize)], names: &'a mut Vec<String>) {
	for (base, start, end) in patterns {
		for i in *start..*end {
			names.push(format!("{}{:02}", base, i));
		}
	}
}

fn generate_name_refs<'a>(names: &'a [String]) -> Vec<&'a str> {
	let mut refs = Vec::new();
	for i in 0..names.len() {
		refs.push(names[i].as_str());
	}
	refs
}

pub fn run_vfs_resmgr_mt_client_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	info!("calling multithreaded resource manager from multiple threads");
	for i in 0..threads.len() {
		let mut thread = threads[i].write();
		thread.run(move || {
			resmgr_test_client("/test_mt/test", MT_CLIENT_TEST_ITERATIONS, false);
			None
		}).expect("cannot start resource manager client thread");
	}
	for i in 0..threads.len() {
		let thread = threads[i].read();
		let _ = thread.get_exit_endpoint().unwrap().recv_refuse_reply();
	}
}

fn resmgr_test_dev_full_link(path: &str, open_flags: u32, resolve_flags: u32){
	info!("opening /dev/full through {}", path);
	let rpc = get_default_rpc_client();
	let how = open_how {
		flags: O_RDWR as u64 | open_flags as u64,
		mode: 0,
		resolve: resolve_flags as u64,
	};
	let full_fd = get_fd(rpc.openat2(AT_FDCWD, path.as_bytes(), &how).expect("could not open test resource manager symlink to /dev/full"));
	let buf = [0u8; 1];
	let err = full_fd.write(&buf).expect_err("write to /dev/full through symlink falsely succeeded").to_errno();
	assert!(err == SRV_ERR_NOSPC, "write to /dev/full through symlink failed with unexpected error {}", err);
	rpc.close(full_fd.get_id()).expect("failed to close /dev/full");
}

fn resmgr_test_readlink(path: &str, target: &str){
	info!("getting target of {}", path);
	let rpc = get_default_rpc_client();
	let mut buf = [0u8; 128];
	let len = rpc.readlinkat(AT_FDCWD, path.as_bytes(), &mut buf).expect("readlinkat() failed");
	assert!(&buf[..len] == target.as_bytes(), "invalid link target for {}", path);
}

fn test_link_fail(path: &str, open_flags: u32, resolve_flags: u32){
	info!("attempting to open symlink {} with O_NOFOLLOW", path);
	let rpc = get_default_rpc_client();
	let how = open_how {
		flags: O_RDWR as u64 | open_flags as u64,
		mode: 0,
		resolve: resolve_flags as u64,
	};
	let err = rpc.openat2(AT_FDCWD, path.as_bytes(), &how).expect_err("attempt to open symlink with O_NOFOLLOW succeeded");
	assert!(err.to_errno() == SRV_ERR_LOOP, "attempt to open symlink with O_NOFOLLOW failed with unexpected error {}", err);
}

fn resmgr_test_links(){
	//TODO: add more extensive tests with links in different places once resolution of dots has been added
	resmgr_test_dev_full_link("/test_st/full", 0, 0);
	resmgr_test_dev_full_link("/test_st/dev/full", 0, 0);
	resmgr_test_dev_full_link("/test_st/full_firm", 0, 0);

	resmgr_test_dev_full_link("//test_st/full", 0, 0);
	resmgr_test_dev_full_link("//test_st/dev/full", 0, 0);
	resmgr_test_dev_full_link("//test_st/full_firm", 0, 0);

	resmgr_test_dev_full_link("/test_st/./full", 0, 0);
	resmgr_test_dev_full_link("/test_st/./dev/full", 0, 0);
	resmgr_test_dev_full_link("/test_st/./full_firm", 0, 0);

	resmgr_test_dev_full_link("/test_st/quux/qux/baz/full0", 0, 0);
	resmgr_test_dev_full_link("/test_st/quux/qux/baz/full1", 0, 0);
	resmgr_test_dev_full_link("/test_st/quux/qux/baz/full2", 0, 0);

	resmgr_test_dev_full_link("/test_st/quux/qux/baz/bar/foo/../../full0", 0, 0);
	resmgr_test_dev_full_link("/test_st/quux/qux/baz/bar/foo/../../full1", 0, 0);
	resmgr_test_dev_full_link("/test_st/quux/qux/baz/bar/foo/../../full2", 0, 0);

	resmgr_test_dev_full_link("/test_st/quux/qux/baz/bar/foo/.../full0", 0, 0);
	resmgr_test_dev_full_link("/test_st/quux/qux/baz/bar/foo/.../full1", 0, 0);
	resmgr_test_dev_full_link("/test_st/quux/qux/baz/bar/foo/.../full2", 0, 0);

	let rpc = get_default_rpc_client();

	info!("trying to chdir to /test_st/quux/qux/baz/bar/foo");
	rpc.chdir("/test_st/quux/qux/baz/bar/foo".as_bytes()).expect("could not chdir to /test_st/quux/qux/baz/bar/foo");
	resmgr_test_dev_full_link("../../../../../../dev/full", 0, 0);
	resmgr_test_dev_full_link("......../dev/full", 0, 0);
	resmgr_test_dev_full_link(".../../../../../dev/full", 0, 0);

	resmgr_test_dev_full_link("../../full0", 0, 0);
	resmgr_test_dev_full_link(".../full0", 0, 0);
	resmgr_test_dev_full_link("../../full1", 0, 0);
	resmgr_test_dev_full_link(".../full1", 0, 0);
	resmgr_test_dev_full_link("../../full2", 0, 0);
	resmgr_test_dev_full_link(".../full2", 0, 0);
	resmgr_test_dev_full_link("../../full2", 0, 0);
	resmgr_test_dev_full_link(".../full2", 0, 0);
	rpc.chdir("/dev".as_bytes()).expect("could not chdir to /dev");

	test_link_fail("/test_st/full", O_NOFOLLOW, 0);
	test_link_fail("/test_st/full", O_NOFOLLOW, RESOLVE_NO_SYMLINKS);
	test_link_fail("/test_st/full", 0, RESOLVE_NO_SYMLINKS);

	test_link_fail("/test_st/dev/full", O_NOFOLLOW, RESOLVE_NO_SYMLINKS);
	test_link_fail("/test_st/dev/full", 0, RESOLVE_NO_SYMLINKS);

	resmgr_test_dev_full_link("/test_st/dev/full", O_NOFOLLOW, 0);
	resmgr_test_dev_full_link("/test_st/full_firm", O_NOFOLLOW, 0);
	resmgr_test_dev_full_link("/test_st/full_firm", O_NOFOLLOW, RESOLVE_NO_SYMLINKS);
	resmgr_test_dev_full_link("/test_st/full_firm", 0, RESOLVE_NO_SYMLINKS);

	resmgr_test_readlink("/test_st/full", "/dev/full");
	resmgr_test_readlink("/test_st/dev", "/dev");
}

pub fn run_vfs_resmgr_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	info!("running resource manager tests");

	let channel_port_id = start_test_resmgrs();

	resmgr_test_links();

	info!("calling single-threaded resource manager from regular path");
	resmgr_test_client("/test_st/test", 1, true);

	info!("calling single-threaded resource manager from relative symlink path");
	resmgr_test_client("/test_st/test_rel_symlink", 1, true);

	info!("calling single-threaded resource manager from global path");
	let global_path = format!("/:mount:{}:{}/test", get_root_default_fs_context().get_id(), channel_port_id);
	resmgr_test_client(&global_path, 1, true);

	info!("calling multithreaded resource manager from regular path");
	resmgr_test_client("/test_mt/test", 1, true);

	let mut mt_expected_names = Vec::new();
	generate_name_list(&[("foo", 0, 64), ("bar", 0, 64)], &mut mt_expected_names);
	let mt_expected_refs = generate_name_refs(&mt_expected_names);

	let mut st_expected_names = Vec::new();
	generate_name_list(&[("baz", 0, 64), ("qux", 0, 64)], &mut st_expected_names);
	let st_expected_refs = generate_name_refs(&st_expected_names);

	info!("testing multithreaded resource manager directories");
	test_dir("/test_mt", &mt_expected_refs, &st_expected_refs);

	info!("testing single-threaded resource manager directories");
	test_dir("/test_st", &st_expected_refs, &mt_expected_refs);

	run_vfs_resmgr_mt_client_tests(threads);
}
