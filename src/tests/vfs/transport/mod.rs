/*
 * Copyright (c) 2022-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module includes core code for testing the VFS and transport layer.
 */

pub mod channel;

use alloc::sync::Arc;
use core::sync::atomic::{AtomicBool, Ordering};

use crate::task::thread::SysThread;

use usync::RwLock;

use uxrt_procfs_client::port::{
	PortFSClient,
	PortType,
};

use uxrt_transport_layer::{
	AccessMode,
	FileDescriptor,
	NOTIFICATION_MSG_SIZE,
};

use crate::vfs::rpc::get_default_rpc_client;

use crate::vfs::transport::get_fd;

use super::allocate_fds;
use super::transport::channel::UNLINK_BEFORE_CLOSE;

use self::channel::{
	run_basic_tests,
	run_extended_tests,
};
use crate::{
	dump_heap,
	dump_utspace,
};

use alloc::vec::Vec;

pub static SERVER_FIRST: AtomicBool = AtomicBool::new(true);
pub static PAUSE_THREADS: AtomicBool = AtomicBool::new(false);
pub static USE_MD_ACCESSORS: AtomicBool = AtomicBool::new(false);

pub fn run_notification_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	let (port_id, server_id, client_id, _, _) = allocate_fds(PortType::Notification, 0, AccessMode::ReadWrite);

	info!("server: {} client: {}", server_id, client_id);

	info!("getting threads");
	let mut client = threads[0].write();

	info!("starting client");
	client.run(move ||{
		let client_fd = get_fd(client_id);
		info!("trying to read from notification FD");
		let mut size = client_fd.readb(NOTIFICATION_MSG_SIZE).expect("could not read from notification FD");
		if size != NOTIFICATION_MSG_SIZE {
			panic!("incorrect message size {} returned from notification FD", size);
		}
		info!("waiting for notification FD to be closed");
		size = client_fd.readb(NOTIFICATION_MSG_SIZE).expect("could not read from notification FD");
		if size != 0 {
			panic!("incorrect message size {} returned from notification FD", size);
		}
		None
	}).expect("could not start client thread");
	sel4::yield_now();

	info!("getting server FD");
	let server_fd = get_fd(server_id);
	info!("signalling notification");
	server_fd.writeb(0).expect("could not write message to notification FD");
	info!("done");
	sel4::yield_now();


	let rpc_client = get_default_rpc_client();
	let portfs_client = PortFSClient::new(None, true);

	info!("deallocating server FD: {}", server_id);
	portfs_client.unlink(port_id).expect("could not unlink notification port");
	rpc_client.close(server_id).expect("could not close server notification connection");

	sel4::yield_now();

	info!("waiting for client to exit");
	client.get_exit_endpoint().unwrap().recv_refuse_reply();
	info!("deallocating client FD");
	rpc_client.close(client_id).expect("could not close client notification connection");

	info!("Heap status after FD deallocation:");
	dump_heap();
	info!("UTSpace status after FD deallocation:");
	dump_utspace();
}

pub fn run_tests_inner_0(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	info!("testing transport layer");

	run_basic_tests(threads);

	run_extended_tests(threads);

	info!("transport layer tests finished");
}

fn run_tests_inner_3(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	info!("running channel tests with unlink before close");
	UNLINK_BEFORE_CLOSE.store(false, Ordering::Relaxed);
	run_tests_inner_2(threads);
	info!("running channel tests with close before unlink");
	UNLINK_BEFORE_CLOSE.store(true, Ordering::Relaxed);
	run_tests_inner_2(threads);
}

fn run_tests_inner_2(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	info!("running channel tests with server first");
	SERVER_FIRST.store(true, Ordering::Relaxed);
	run_tests_inner_1(threads);
	info!("running channel tests with client first");
	SERVER_FIRST.store(false, Ordering::Relaxed);
	run_tests_inner_1(threads);
}

fn run_tests_inner_1(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	info!("running channel tests with message descriptor accessors disabled");
	USE_MD_ACCESSORS.store(false, Ordering::Relaxed);
	run_tests_inner_0(threads);
	info!("running channel tests with message descriptor accessors enabled");
	USE_MD_ACCESSORS.store(true, Ordering::Relaxed);
	run_tests_inner_0(threads);
}

pub fn run_transport_layer_tests(threads: &mut Vec<Arc<RwLock<SysThread>>>){
	info!("running transport layer tests");
	info!("running channel tests without pause");
	PAUSE_THREADS.store(false, Ordering::Relaxed);
	run_tests_inner_3(threads);
	info!("running channel tests with pause");
	PAUSE_THREADS.store(true, Ordering::Relaxed);
	run_tests_inner_3(threads);
	info!("running notification tests");
	run_notification_tests(threads);
}
/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
