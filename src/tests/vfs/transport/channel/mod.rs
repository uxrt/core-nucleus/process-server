/*
 * Copyright (c) 2022-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 * This module includes core code for testing the VFS and transport layer.
 */

pub mod basic;
pub mod extended;
pub mod random;
pub use self::basic::run_basic_tests;
pub use extended::run_extended_tests;
use self::extended::deallocate_extended_test_buffers;

use sel4_thread::{
	ThreadReturn,
	WrappedThread,
};

use alloc::sync::Arc;
use core::sync::atomic::{AtomicBool, Ordering};
use crate::global_heap_alloc::get_kobj_alloc;

use crate::task::thread::SysThread;

use crate::tests::vfs::allocate_fds;

use crate::vfs::rpc::{
	get_default_rpc_client,
	get_root_default_fs_context,
};

use uxrt_procfs_client::{
	PORT_CHANNEL_GET_RECV_TOTAL_SIZE,
	PORT_CHANNEL_GET_REPLY_SIZE,
};

use uxrt_procfs_client::port::{
	PortFSClient,
	PortType,
};

use usync::RwLock;

use uxrt_transport_layer::{
	AccessMode,
	ServerChannelOptions,
};

use crate::{
	dump_heap,
	dump_utspace,
};

pub use self::extended::handle_extended_test_page_fault;


use alloc::vec::Vec;

pub static SERVER_FIRST: AtomicBool = AtomicBool::new(true);
pub static PAUSE_THREADS: AtomicBool = AtomicBool::new(false);
pub static UNLINK_BEFORE_CLOSE: AtomicBool = AtomicBool::new(false);

pub fn channel_test_wait(client: &SysThread, server: &SysThread) -> usize {
	channel_test_wait_secondary(client, server, None)
}

pub fn channel_test_wait_secondary(client: &SysThread, server: &SysThread, secondary_server: Option<&SysThread>) -> usize {
	let mut i = 0;
	for _ in 0..10000000 {
		i += 1;
	}
	if PAUSE_THREADS.load(Ordering::Relaxed){
		for _ in 0..10000000{
			i += 1;
		}
		server.pause().expect("failed to pause server");
		if let Some(thread) = secondary_server {
			thread.pause().expect("failed to pause secondary server");
		}
		client.pause().expect("failed to pause client");
		i = 0;
		for _ in 0..10000000{
			i += 1;
		}
		if SERVER_FIRST.load(Ordering::Relaxed){
			server.resume().expect("failed to resume server");
			if let Some(thread) = secondary_server {
				thread.resume().expect("failed to pause secondary server");
			}
		}else{
			client.resume().expect("failed to resume client");
		}
	}
	i
}

pub fn run_channel_test_send_size_only(server_fn: fn(i32, i32), client_fn: fn(i32), threads: &mut Vec<Arc<RwLock<SysThread>>>, access: AccessMode) {
	run_channel_test_closure(move |server_channel_id, server_message_id|{
		server_fn(server_channel_id, server_message_id);
	},
	move |client_id|{
		client_fn(client_id);
	},
	threads, access,
	ServerChannelOptions::new());
}

pub fn run_channel_test(server_fn: fn(i32, i32), client_fn: fn(i32), threads: &mut Vec<Arc<RwLock<SysThread>>>, access: AccessMode) {
	run_channel_test_closure(move |server_channel_id, server_message_id|{
		server_fn(server_channel_id, server_message_id);
	},
	move |client_id|{
		client_fn(client_id);
	},
	threads, access,
	ServerChannelOptions::new().get_reply_size().get_recv_total_size());
}

pub fn run_channel_test_closure<F, G>(mut server_fn: F, mut client_fn: G, threads: &mut Vec<Arc<RwLock<SysThread>>>, access: AccessMode, options: ServerChannelOptions)
	where F: FnMut(i32, i32) + Send + 'static,
	G: FnMut(i32) + Send + 'static {
	let (channel_port_id, server_channel_id, client_id, server_message_id, message_port_id) = allocate_fds(PortType::DirectChannel, (PORT_CHANNEL_GET_REPLY_SIZE | PORT_CHANNEL_GET_RECV_TOTAL_SIZE) as u64, access);

	info!("getting threads");
	let mut server = threads[0].write();
	let mut client = threads[1].write();

	let server_wrapper = move || -> Option<ThreadReturn>{
		server_fn(server_channel_id, server_message_id);
		None
	};

	let client_wrapper = move || -> Option<ThreadReturn>{
		client_fn(client_id);
		None
	};
	if SERVER_FIRST.load(Ordering::Relaxed) {
		info!("starting server");
		server.run(server_wrapper).expect("could not start server thread");
		channel_test_wait(&client, &server);
		info!("starting client");
		client.run(client_wrapper).expect("could not start client thread");
	}else{
		info!("starting client");
		client.run(client_wrapper).expect("could not start client thread");
		channel_test_wait(&client, &server);
		info!("starting server");
		server.run(server_wrapper).expect("could not start server thread");
	}

	if PAUSE_THREADS.load(Ordering::Relaxed){
		for _ in 0..100{
			client.pause().expect("failed to pause client");
			server.pause().expect("failed to pause server");
			sel4::yield_now();
			for _ in 0..1000000{
			}
			client.resume().expect("failed to pause client");
			server.resume().expect("failed to pause server");
			sel4::yield_now();
		}
	}
	client.get_exit_endpoint().unwrap().recv_refuse_reply();
	client.suspend().expect("failed to suspend client");
	info!("client done");

	let rpc_client = get_default_rpc_client();
	let portfs_client = PortFSClient::new(None, true);

	if UNLINK_BEFORE_CLOSE.load(Ordering::Relaxed) {
		info!("unlinking channel port");
		portfs_client.unlink(channel_port_id).expect("cannot unlink channel port");
		info!("closing client FD");
		rpc_client.close(client_id).expect("cannot close client FD");
	}else{
		info!("closing client FD");
		rpc_client.close(client_id).expect("cannot close client FD");
		info!("unlinking channel port");
		portfs_client.unlink(channel_port_id).expect("cannot unlink channel port");
	}

	let context = get_root_default_fs_context();
	let ports = context.get_ports();
	assert!(ports.port_is_valid(channel_port_id), "channel port is invalid");
	assert!(ports.port_is_unlinked(channel_port_id), "channel port is not unlinked after destroy");

	if server.get_sched_context().is_none() {
		info!("binding scheduling context");
		let kobj_alloc = get_kobj_alloc();
		server.new_sched_context(&kobj_alloc).expect("could not create scheduling context");
	}else{
		info!("waiting for server to exit");
		server.get_exit_endpoint().unwrap().recv_refuse_reply();
	}
	info!("suspending server thread");
	server.suspend().expect("cannot suspend server thread");


	rpc_client.close(server_channel_id).expect("cannot close server channel FD");
	portfs_client.unlink(message_port_id).expect("cannot unlink message port");
	rpc_client.close(server_message_id).expect("cannot close server message FD");

	assert!(!ports.port_is_valid(channel_port_id), "channel port is valid after all channels closed");
	assert!(ports.port_is_unlinked(channel_port_id), "channel port is not unlinked after all channels closed");

	deallocate_extended_test_buffers();
	info!("Heap status after FD deallocation:");
	dump_heap();
	info!("UTSpace status after FD deallocation:");
	dump_utspace();
}

/* vim: set softtabstop=8 tabstop=8 shiftwidth=8 noexpandtab */
