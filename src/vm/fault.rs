/*
 * Copyright (c) 2022-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */
///This is the base fault handler. This dispatches faults to the appropriate
///driver if they can be handled and panics otherwise

use sel4::{
	FaultMsg,
	RecvToken,
	Reply,
};

use sel4_thread::WrappedThread;

use crate::task::get_task_tree;

use super::{
	decode_fault_badge,
	get_root_alloc,
};

static FAULT_HANDLER: FaultHandler = FaultHandler::new();

#[cfg(feature = "test_vfs")]
use crate::tests::vfs::handle_extended_test_page_fault;

#[cfg(not(feature = "test_vfs"))]
pub fn handle_extended_test_page_fault(addr: usize) {
	panic!("unhandled page fault at address {}", addr);
}

///Gets the fault handler
pub fn get_fault_handler() -> &'static FaultHandler {
	&FAULT_HANDLER
}

///Global handler for thread faults
pub struct FaultHandler {
}

impl FaultHandler {
	///Create a new `FaultHandler`
	const fn new() -> FaultHandler {
		FaultHandler {}
	}
	///Initializes the fault handler
	///
	///This creates a thread that runs the main_loop() method
	pub fn init(&self){
		//TODO: add a separate double fault handler that just panics
		info!("initializing fault handler");
		let f = get_task_tree().new_root_thread(None).expect("could not create fault handler thread");
		let mut fault_handler = f.write();
		fault_handler.set_name("fault_handler");
		fault_handler.run(move ||{
			get_fault_handler().main_loop();
		}).expect("failed to start fault handler thread");
	}
	///Main loop of the fault handler
	fn main_loop(&self) -> ! {
		let fault_endpoint = get_root_alloc().get_orig_fault_endpoint();
		let fault_reply = get_root_alloc().get_fault_reply();
		loop {
			let (fault, msg) = FaultMsg::recv(fault_endpoint, fault_reply);
			self.handle_fault(fault, msg, fault_reply);
		}
	}
	///Handles an individual fault.
	///
	///Currently just panics the entire system
	fn handle_fault(&self, fault: Option<FaultMsg>, msg: RecvToken, reply: Reply){
		let (pid, tid) = decode_fault_badge(msg.badge);

		info!("received fault in process {} thread {}: {:?}", pid, tid, fault);
		match fault.expect("null fault message received"){
			FaultMsg::IpcVmFault(token) => {
				if token.msg.addr == 0 {
					token.resolve_fault_fail(reply);
				}else{
					handle_extended_test_page_fault(token.msg.addr);
					token.resolve_fault(reply);
				}
			},
			FaultMsg::VmFault(token) => {
				handle_extended_test_page_fault(token.msg.addr);
				token.resolve_fault(reply);
			},
			_ => { panic!("unhandled fault in process {} thread {}: {:?}", pid, tid, fault) },
		}

		//TODO: handle FDSpace faults here (check if the fault is in an FDSpace array here and page in the dummy page, passing it on (i.e. just panicking at the moment) otherwise)
	}
}

/* vim: set softtabstop=8 tabstop=8 noexpandtab: */
