/*
 * Copyright (c) 2024-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///This is the vnode list

use core::str;

use alloc::sync::Arc;

use usync::RwLock;

use usync_once_cell::OnceCell;

use intrusive_collections::{
	KeyAdapter,
	RBTree,
	RBTreeAtomicLink,
};

use uxrt_transport_layer::{
	SRV_ERR_IO,
	ServerStatus,
};

use uxrt_vfs_rpc::{
	dev_t,
	ino_t,
};

use crate::add_arc_slab;

use crate::drivers::portfs::GlobalPortID;

use super::transport::FileDescription;

intrusive_adapter!(FileDescriptionVnodeAdapter = Arc<FileDescription>: FileDescription { vnode_link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for FileDescriptionVnodeAdapter {
	type Key = (u64, i32);
	fn get_key(&self, desc: &'a FileDescription) -> (u64, i32) {
		(desc.get_global_id(), desc.get_connection_id())
	}
}

///State common to all `FileDescription`s associated with a particular port and
///inode
pub struct Vnode {
	port: u64,
	inode: u64,
	descs: RwLock<RBTree<FileDescriptionVnodeAdapter>>,
	main_link: RBTreeAtomicLink,
}

impl Vnode {
	///Creates a new `Vnode`
	pub fn new(port: u64, inode: u64) -> Arc<Self> {
		Arc::new(Self {
			port,
			inode,
			descs: Default::default(),
			main_link: Default::default(),
		})
	}
	///Adds a new `FileDescription` to this `Vnode`
	pub fn add_desc(&self, desc: Arc<FileDescription>) -> Result<(), ServerStatus> {
		//info!("Vnode::add_desc: {} {}", desc.get_global_id(), desc.get_inode());
		if desc.get_global_id() == GlobalPortID::invalid().value() {
			warn!("Vnode::add_desc: port ID unset: {:x}", desc.get_global_id());
			return Err(SRV_ERR_IO);
		}
		if !desc.vnode_link.is_linked(){
			self.descs.write().insert(desc);
		}
		Ok(())
	}
	///Removes a `FileDescription` from this `Vnode`
	pub fn remove_desc(&self, port: u64, conn: i32) -> Option<Arc<FileDescription>> {
		//info!("Vnode::remove_desc: {} {}", port, conn);
		let mut descs = self.descs.write();
		let mut cursor = descs.find_mut(&(port, conn));
		let res = cursor.remove();
		let is_null = descs.front().is_null();
		drop(descs);
		if is_null {
			//info!("Vnode::remove_desc: removing vnode {} {} from list", self.port, self.inode);
			get_vnode_list().write().remove(self.port, self.inode);
		};
		res

	}
	///Gets the port number of this `Vnode`
	pub fn get_port_id(&self) -> dev_t {
		self.port as dev_t
	}
	///Gets the inode number of this `Vnode`
	pub fn get_inode_id(&self) -> ino_t {
		self.inode as ino_t
	}
}

intrusive_adapter!(VnodeAdapter = Arc<Vnode>: Vnode { main_link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for VnodeAdapter {
	type Key = (u64, u64);
	fn get_key(&self, vnode: &'a Vnode) -> (u64, u64) {
		(vnode.port, vnode.inode)
	}
}

///The list of all `Vnode`s
pub struct VnodeList {
	contents: RBTree<VnodeAdapter>,
}

impl VnodeList {
	///Creates a new `VnodeList`
	fn new() -> Self {
		Self {
			contents: Default::default(),
		}
	}
	pub fn add_desc(&mut self, desc: &Arc<FileDescription>, port: u64, inode: u64) -> Result<(), ServerStatus> {
		let vnode = if let Some(v) = self.get(port, inode){
			v
		}else{
			let v = Vnode::new(port, inode);
			self.add(v.clone());
			v
		};
		desc.set_vnode(&vnode)?;
		Ok(())
	}
	///Adds a `Vnode` to the list
	pub fn add(&mut self, vnode: Arc<Vnode>){
		self.contents.insert(vnode);
	}
	///Gets a `Vnode` from the list
	pub fn get(&mut self, port: u64, inode: u64) -> Option<Arc<Vnode>> {
		let cursor = self.contents.find(&(port, inode));
		cursor.clone_pointer()
	}
	///Removes a `Vnode` from the list
	pub fn remove(&mut self, port: u64, inode: u64) -> Option<Arc<Vnode>> {
		let mut cursor = self.contents.find_mut(&(port, inode));
		cursor.remove()
	}
}

static VNODE_LIST: OnceCell<RwLock<VnodeList>> = OnceCell::new();

///Gets the vnode list
pub fn get_vnode_list() -> &'static RwLock<VnodeList> {
	VNODE_LIST.get_or_init(|| {
		RwLock::new(VnodeList::new())
	})
}

///Adds VNode-related custom slabs
pub fn add_custom_slabs() {
	add_arc_slab!(Vnode, 512, 4, 4, 2, "vnode");
}
