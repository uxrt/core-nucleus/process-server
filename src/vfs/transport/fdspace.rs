/*
 * Copyright (c) 2022-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

///FDSpaces and related state

use core::sync::atomic::{
	AtomicBool,
	AtomicI32,
	AtomicUsize,
	Ordering,
};

use alloc::boxed::Box;
use alloc::sync::Arc;

use sel4::{
	CNode,
	CNodeInfo,
	Endpoint,
	FromSlot,
	Notification,
	PAGE_BITS,
	Reply,
	SlotRef,
	Window,
	seL4_CPtr,
};

use sel4_alloc::AllocatorBundle;
use sel4_alloc::cspace::{
	CSpaceError,
	CSpaceManager,
	DynamicBitmapAllocator,
};

use sel4_alloc::utspace::UtZone;

use intrusive_collections::{
	Bound,
	KeyAdapter,
	RBTree,
	RBTreeAtomicLink,
	UnsafeRef,
	rbtree,
};

use crate::utils::{
	LockedArcList,
	ArcListItem,
};

use crate::vm::vspace::{
	MemRights,
	VRegionFactory,
	VSpaceContainer,
	get_root_vspace,
};

use crate::global_heap_alloc::{
	AllocatorBundleGuard,
	get_kobj_alloc,
};

use crate::{
	add_arc_slab,
	add_arc_list_slab,
};

use crate::utils::add_slab;

use usync::{
	MutexGuard,
	Mutex,
	RwLock,
	RwLockReadGuard,
	RwLockWriteGuard
};

use uxrt_transport_layer::{
	AccessMode,
	CLIENT_ENDPOINT_COPIES,
	FD_ALIGN,
	FDArray,
	FileDescriptorRef,
	IOError,
	RawFileDescriptor,
	UnifiedFileDescriptor,
	get_reserved_index,
	set_fd_arrays,
};

use super::super::VFSError;

use super::allocate_id;

use super::description::{
	FileDescription,
	FileDescriptorType,
	FileDescriptionType,
};

///Internal state of a file descriptor
pub struct FDInternal {
	fdspace_id: i32,
	fd_id: i32,
	fd_type: FileDescriptorType,
	access: AccessMode,
	file_description: Arc<FileDescription>,
	cspace: FDCSpace,
	user_slots: [Option<SlotRef>; CLIENT_ENDPOINT_COPIES],
	badge: u32,
	user_fd_ptr: AtomicUsize,
	closed: AtomicBool,
	fdspace_link: RBTreeAtomicLink,
}


impl FDInternal {
	///Gets the ID of this FD
	pub(super) fn get_id(&self) -> i32 {
		self.fd_id
	}
	///Gets the type of this file descriptor
	pub(super) fn get_fd_type(&self) -> FileDescriptorType {
		self.fd_type
	}
	///Gets the global ID of this FD
	pub(super) fn get_global_id(&self) -> u64 {
		((self.fdspace_id as u64) << 32) | self.fd_id as u64
	}
	pub fn get_file_description(&self) -> Arc<FileDescription> {
		self.file_description.clone()
	}
	pub fn get_rpc_io_fd(&self) -> Result<(UnifiedFileDescriptor, i32), VFSError> {
		self.file_description.get_rpc_io_fd(self.fd_type)
	}
	///Gets the user-level struct for this file descriptor
	unsafe fn get_user_fd(&self) -> UnsafeRef<UnifiedFileDescriptor> {
		unsafe { UnsafeRef::from_raw(self.user_fd_ptr.load(Ordering::Relaxed) as *const UnifiedFileDescriptor) }
	}
	///Called when a file descriptor is added to the client side
	fn added_client(&mut self) -> Result<UnifiedFileDescriptor, VFSError> {
		let res = match self.file_description.cap_info.base_type {
			FileDescriptionType::IPCChannel(_) => {
				let mut slots = [None; CLIENT_ENDPOINT_COPIES];
				for i in 0..self.user_slots.len() {
					if let Some(slot) = self.user_slots[i] {
						slots[i] = Some(Endpoint::from_slot(slot));

					}
				}
				Ok(UnifiedFileDescriptor::new_client(slots, self.file_description.access))
			},
			FileDescriptionType::ServerMessage => {
				warn!("attempt to add a client-side FD for a server message description");
				Err(VFSError::InternalError)
			},
			FileDescriptionType::Notification => {
				Ok(UnifiedFileDescriptor::new_notification(Notification::from_slot(self.user_slots[0].expect("no slot for endpoint notification; this shouldn't happen!")), self.file_description.access))
			}
		};
		res
	}
	///Called when a file descriptor is added to the server side
	fn added_server(&mut self) -> Result<UnifiedFileDescriptor, VFSError> {
		let slot = self.user_slots[0].expect("no slot for server FD; this shouldn't happen!");
		let res = match self.file_description.cap_info.base_type {
			FileDescriptionType::IPCChannel(_) => {
				Ok(UnifiedFileDescriptor::new_server_channel(Endpoint::from_slot(slot), self.file_description.access, self.file_description.cap_info.server_options))
			},
			FileDescriptionType::ServerMessage => {
				Ok(UnifiedFileDescriptor::new_server_message(Reply::from_slot(slot)))
			},
			FileDescriptionType::Notification => {
				Ok(UnifiedFileDescriptor::new_notification(Notification::from_slot(slot), self.file_description.access))
			}
		};
		res
	}
	///Called when this is added to an FDSpace. Returns the user file
	///descriptor
	fn added(&mut self) -> Result<UnifiedFileDescriptor, VFSError> {
		self.user_slots = self.file_description.cap_info.mint_caps(&self.cspace, self.fd_type, self.access, self.badge, self.file_description.get_connection_id())?;
		let res = match self.fd_type {
			FileDescriptorType::Client => {
				self.added_client()
			},
			FileDescriptorType::Server => {
				self.added_server()
			}
		};
		res
	}
	///Close this connection from a local context
	pub(super) fn close_local(&self) -> Result<(), VFSError> {
		self.close_inner(true)
	}
	///Close this connection from a remote context
	pub(super) fn close_remote(&self) -> Result<(), VFSError> {
		self.close_inner(false)
	}
	///Inner implementation of close_local and close_remote
	fn close_inner(&self, is_local: bool) -> Result<(), VFSError> {
		//info!("close_inner: {}", is_local);
		unsafe { self.get_user_fd().get_base_fd().clunk(); }
		let mut local_slots = false;
		if let FDCSpace::Root = self.cspace && let FileDescriptionType::IPCChannel(_) = self.file_description.cap_info.base_type && self.fd_type.is_client() {
			local_slots = true;
		}

		let mut res = Ok(());
		if !self.closed.swap(true, Ordering::Relaxed) && local_slots {
			for opt in self.user_slots {
				if let Some(slot) = opt {
					if let Err(err) = slot.delete() {
						res = Err(VFSError::CSpaceError(CSpaceError::SyscallError { details: err }));
					}
				}
			}
		}

		if is_local && local_slots {
			let kobj_alloc = get_kobj_alloc();
			let cspace = self.cspace.lock();
			for opt in self.user_slots {
				if let Some(slot) = opt {
					//info!("close_inner {:p}: freeing slot {:?}", self, slot);
					let dealloc_res = cspace.free_slot(slot, &kobj_alloc);
					if let Err(err) = dealloc_res {
						res = Err(VFSError::CSpaceError(err));
					}
				}
			}
		}
		res
	}
}

intrusive_adapter!(FDSpaceFDAdapter = Arc<FDInternal>: FDInternal { fdspace_link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for FDSpaceFDAdapter {
	type Key = i32;
	fn get_key(&self, fd: &'a FDInternal) -> i32 {
		fd.get_id()
	}
}


///An entry in the list of VSpaces associated with a `FileDescription`
struct FDVSpace {
	contents: Arc<VSpaceContainer>,
	num_fdspaces: AtomicUsize,
	link: RBTreeAtomicLink,
}

impl FDVSpace {
	///Creates a new `FDVSpace`
	pub fn new(contents: Arc<VSpaceContainer>) -> Arc<FDVSpace> {
		Arc::new(FDVSpace {
			contents,
			num_fdspaces: AtomicUsize::new(1),
			link: Default::default(),
		})
	}
	///Increments the number of FDSpaces associated with this VSpace
	fn inc_fdspaces(&self) {
		self.num_fdspaces.fetch_add(1, Ordering::Relaxed);
	}
	///Decrements the number of FDSpace associated with this VSpace
	fn dec_fdspaces(&self) -> usize {
		self.num_fdspaces.fetch_sub(1, Ordering::Relaxed) - 1
	}
}

intrusive_adapter!(FDVSpaceAdapter = Arc<FDVSpace>: FDVSpace { link: RBTreeAtomicLink });

impl<'a> KeyAdapter<'a> for FDVSpaceAdapter {
	type Key = i32;
	fn get_key(&self, vspace: &'a FDVSpace) -> i32 {
		vspace.contents.get_id()
	}
}

///The CSpace associated with a `FactoryFDSpace`
pub enum FDCSpace {
	Root,
	User(Arc<Mutex<DynamicBitmapAllocator>>),
}

impl FDCSpace {
	///Acquires the mutex to the CSpace, returning a guard
	pub fn lock(&self) -> FDCSpaceGuard {
		match self {
			FDCSpace::Root => {
				FDCSpaceGuard::Root(get_kobj_alloc())
			},
			FDCSpace::User(ref cspace) => {
				FDCSpaceGuard::User(cspace.lock())
			},
		}
	}
}
impl Clone for FDCSpace {
	fn clone(&self) -> Self {
		match self {
			FDCSpace::Root => FDCSpace::Root,
			FDCSpace::User(ref contents) => FDCSpace::User(contents.clone()),
		}
	}
}

impl Drop for FDCSpace {
	fn drop(&mut self) {
		match self {
			FDCSpace::Root => {},
			FDCSpace::User(_) => {
				let kobj_alloc = get_kobj_alloc();
				kobj_alloc.cspace().free_and_delete_sublevel(self.lock(), &kobj_alloc).expect("could not free user CSpace");
			},
		}
	}
}

///A guard to a `FDCSpace`, which wraps the underlying CSpace allocator
///(the heap CSpace allocator for the root server FDSpace, and a dedicated on
///for each user FDSpace)
pub enum FDCSpaceGuard<'a> {
	Root(AllocatorBundleGuard<'a>),
	User(MutexGuard<'a, DynamicBitmapAllocator>),
}

impl<'a> CSpaceManager for FDCSpaceGuard<'a> {
	fn allocate_slot<A: AllocatorBundle>(&self, alloc: &A) -> Result<SlotRef, CSpaceError> {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().allocate_slot(alloc),
			FDCSpaceGuard::User(ref guard) => guard.allocate_slot(alloc),
		}
	}
	fn allocate_slot_raw<A: AllocatorBundle>(&self, alloc: &A) -> Result<seL4_CPtr, CSpaceError> {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().allocate_slot_raw(alloc),
			FDCSpaceGuard::User(ref guard) => guard.allocate_slot_raw(alloc),
		}
	}
	fn free_slot_raw<A: AllocatorBundle>(&self, cptr: seL4_CPtr, alloc: &A) -> Result<(), CSpaceError> {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().free_slot_raw(cptr, alloc),
			FDCSpaceGuard::User(ref guard) => guard.free_slot_raw(cptr, alloc),
		}
	}
	fn slot_info_raw(&self, cptr: seL4_CPtr) -> Option<CNodeInfo> {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().slot_info_raw(cptr),
			FDCSpaceGuard::User(ref guard) => guard.slot_info_raw(cptr),
		}
	}
	fn slot_window_raw(&self, cptr: seL4_CPtr) -> Option<Window> {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().slot_window_raw(cptr),
			FDCSpaceGuard::User(ref guard) => guard.slot_window_raw(cptr),
		}
	}
	fn window(&self) -> Option<Window> {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().window(),
			FDCSpaceGuard::User(ref guard) => guard.window(),
		}
	}
	fn info(&self) -> Option<CNodeInfo> {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().info(),
			FDCSpaceGuard::User(ref guard) => guard.info(),
		}
	}
	fn slots_remaining(&self) -> usize {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().slots_remaining(),
			FDCSpaceGuard::User(ref guard) => guard.slots_remaining(),
		}
	}
	fn num_slots(&self) -> usize {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().num_slots(),
			FDCSpaceGuard::User(ref guard) => guard.num_slots(),
		}
	}
	fn parent_root(&self) -> Option<CNode> {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().parent_root(),
			FDCSpaceGuard::User(ref guard) => guard.parent_root(),
		}
	}
	fn minimum_slots(&self) -> usize {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().minimum_slots(),
			FDCSpaceGuard::User(ref guard) => guard.minimum_slots(),
		}
	}
	fn minimum_untyped(&self) -> usize {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().minimum_untyped(),
			FDCSpaceGuard::User(ref guard) => guard.minimum_untyped(),
		}
	}
	fn minimum_vspace(&self) -> usize {
		match self {
			FDCSpaceGuard::Root(ref guard) => guard.cspace().minimum_vspace(),
			FDCSpaceGuard::User(ref guard) => guard.minimum_vspace(),
		}
	}
}

///A list of file descriptors associated with one or more `FSContext`s
pub struct FDSpace {
	id: i32,
	cspace: FDCSpace,
	max_size: i32,
	search_start: i32,
	allocated: usize,
	fds_internal: RBTree<FDSpaceFDAdapter>,
	fds_user: UnsafeRef<FDArray>,
	fds_root: Mutex<FDArray>,
	fds_reserved: RBTree<FDSpaceFDAdapter>,
	region: Arc<Mutex<VRegionFactory>>,
	vspaces: Mutex<RBTree<FDVSpaceAdapter>>,
	root_start_vaddr: AtomicUsize,
}

impl FDSpace {
	///Creates an `FDSpace` for the root server
	pub fn new_root(id: i32, initial_size: usize, max_size: usize) -> Result<FDSpace, VFSError> {
		let (start_vaddr, initial_size, region) = reserve_array(initial_size, FD_ALIGN)?;
		//safety: reserve_array always returns a valid region address
		//and size
		let fds_user = unsafe { UnsafeRef::from_box(Box::new(FDArray::new(initial_size, start_vaddr))) };

		fds_user.check_struct_layout();
		let fds_root = FDArray::clone(&fds_user);

		Ok(FDSpace {
			id,
			cspace: FDCSpace::Root,
			max_size: max_size as i32,
			search_start: 0,
			allocated: 0,
			fds_internal: Default::default(),
			fds_user,
			fds_root: Mutex::new(fds_root),
			fds_reserved: Default::default(),
			vspaces: Mutex::new(Default::default()),
			region,
			root_start_vaddr: AtomicUsize::new(start_vaddr),
		})
	}
	///Sets the maximum size of the FDSpace
	pub fn set_max_size(&mut self, size: i32){
		self.max_size = size;
	}
	///Adds an FD to the FDSpace
	///
	///If `id` is Some, the FD will be added with that ID (if an FD with
	///that ID already exists, it will be closed).
	pub fn insert(&mut self, desc: &Arc<FileDescription>, fd_type: FileDescriptorType, access: AccessMode, badge: u32, id: Option<i32>) -> Result<i32, VFSError> {
		//info!("FDSpace::insert: {:?} {} {:?}", fd_type, badge, id);
		let new_id = if let Some(existing_id) = id {
			let c = self.fds_internal.find_mut(&existing_id);
			if !c.is_null(){
				self.remove(existing_id)?;
			};
			existing_id
		}else{
			if self.allocated >= self.max_size as usize {
				return Err(VFSError::TooManyFiles);
			}

			let new_id = if let Some(i) = allocate_id!(self.fds_internal, self.search_start) {
				i
			}else{
				return Err(VFSError::TooManyFiles);
			};
			new_id
		};
		let mut fds_root = self.fds_root.lock();
		if new_id >= fds_root.len() as i32 {
			panic!("TODO: implement FDSpace expansion");
			//TODO: make sure to update the size of fds_user as well as fds_root here
		}

		self.allocated += 1;
		//info!("FDSpace::insert: allocated: {}", self.allocated);
		//info!("FDSpace::insert: creating internal info: {:?}", fd_type);

		let mut fd = FDInternal {
			fdspace_id: self.id,
			fd_id: new_id,
			file_description: desc.clone(),
			access,
			cspace: self.cspace.clone(),
			user_slots: [None; CLIENT_ENDPOINT_COPIES],
			fd_type,
			closed: AtomicBool::new(false),
			badge,
			user_fd_ptr: AtomicUsize::new(0),
			fdspace_link: Default::default(),
		};

		//info!("FDSpace::insert: id: {} {}", self.id, new_id);
		let user_fd = fd.added()?;

		let arc_fd = Arc::new(fd);
		self.fds_internal.insert(arc_fd.clone());
		arc_fd.file_description.conn_info.add(arc_fd.clone())?;
		fds_root.insert(new_id, user_fd);
		arc_fd.user_fd_ptr.store(fds_root.get(new_id) as *const UnifiedFileDescriptor as usize, Ordering::Relaxed);
		Ok(new_id)
	}
	///For a regular file descriptor, gets the FileDescription-wide RPC
	///control file descriptor for sending RPC messages to the server
	pub fn get_control_fd(&self, id: i32) -> Option<UnifiedFileDescriptor> {
		if let Some(fd) = self.get_internal(id) {
			fd.file_description.cap_info.control_fd.clone()
		}else{
			None
		}
	}
	///Duplicates an FD to another FDSpace (given as a reference to the
	///lock). The resulting FD refers to the same FileDescription as the
	///original.
	pub fn dup_external(&self, src_id: i32, dest: Arc<RwLock<FDSpace>>, dest_id: Option<i32>, fd_type: Option<FileDescriptorType>, badge: Option<u32>) -> Result<i32, VFSError> {
		let mut guard = dest.write();
		self.dup_external_locked(src_id, &mut guard, dest_id, fd_type, badge)
	}
	///Duplicates an FD to another FDSpace (given as a lock guard). The
	///resulting FD refers to the same FileDescription as the original.
	pub fn dup_external_locked<'a>(&self, src_id: i32, dest: &mut RwLockWriteGuard<'a, FDSpace>, dest_id: Option<i32>, fd_type: Option<FileDescriptorType>, badge: Option<u32>) -> Result<i32, VFSError> {
		let cursor = self.fds_internal.find(&src_id);
		if let Some(src) = cursor.get() {
			let dest_type = if let Some(t) = fd_type {
				t
			}else{
				src.fd_type
			};
			let dest_badge = if let Some(b) = badge {
				b
			}else{
				src.badge
			};
			dest.insert(&src.file_description, dest_type, src.file_description.access, dest_badge, dest_id)
		}else{
			Err(VFSError::IOError(IOError::InvalidOperation))
		}
	}
	///Duplicates an FD within this FDSpace. The resulting FD refers to
	///the same FileDescription as the original.
	pub fn dup(&mut self, src_id: i32, dest_id: Option<i32>, fd_type: Option<FileDescriptorType>, badge: Option<u32>) -> Result<i32, VFSError> {
		let cursor = self.get_internal_cursor(src_id);
		let src_opt = cursor.clone_pointer();
		drop(cursor);
		if let Some(src) = src_opt {
			let dest_type = if let Some(t) = fd_type {
				t
			}else{
				src.fd_type
			};
			let dest_badge = if let Some(b) = badge {
				b
			}else{
				src.badge
			};
			self.insert(&src.file_description, dest_type, src.file_description.access, dest_badge, dest_id)
		}else{
			Err(VFSError::IOError(IOError::InvalidOperation))
		}
	}

	///Removes an FD from this FDSpace.
	pub fn remove(&mut self, id: i32) -> Result<(), VFSError>{
		//info!("FDSpace::remove: {} {}", id, self.allocated);
		let mut cursor = self.fds_internal.find_mut(&id);
		if let Some(fd) = cursor.remove(){
			if id < self.search_start {
				self.search_start = id;
			}

			let res = fd.file_description.remove(fd.fd_type, fd.get_global_id());
			let mut fds_root = self.fds_root.lock();
			fds_root.remove(id as i32);

			self.allocated -= 1;
			res?;
			Ok(())
		}else{
			Err(VFSError::IOError(IOError::InvalidOperation))
		}
	}
	///Internal method to get an immutable cursor to the list of internal
	///file descriptor state
	fn get_internal_cursor(&self, id: RawFileDescriptor) -> rbtree::Cursor<'_, FDSpaceFDAdapter> {
		self.fds_internal.find(&id)
	}
	///Gets the internal state for a file descriptor
	pub fn get_internal(&self, id: RawFileDescriptor) -> Option<&FDInternal> {
		let cursor = self.get_internal_cursor(id);
		cursor.get()
	}
	///Gets the user-visible array of FDs
	pub fn get_user_fds(&self) -> UnsafeRef<FDArray> {
		self.fds_user.clone()
	}
	///Adds a VSpace to this FDSpace if it isn't already in the list
	pub fn add_vspace(&self, vspace: Arc<VSpaceContainer>) -> Result<(), VSpaceContainer> {
		let mut vspaces = self.vspaces.lock();

		if let Some(existing_vspace) = vspaces.find(&vspace.get_id()).get(){
			existing_vspace.inc_fdspaces();
			return Ok(());
		}
		let fd_vspace = FDVSpace::new(vspace);
		vspaces.insert(fd_vspace.clone());

		panic!("TODO: fully implement add_vspace");
		//TODO: actually map the user array into the destination VSpace if it's a user VSpace
	}
	///Removes a VSpace from this `FDSpace`
	pub fn remove_vspace(&self, id: i32) -> Result<(), VFSError> {
		let mut vspaces = self.vspaces.lock();
		let mut vspace_cursor = vspaces.find_mut(&id);
		let existing_vspace = if let Some(v) = vspace_cursor.get(){
			v
		}else{
			warn!("attempted to remove VSpace with ID {} from FileDescription {:p}, but it was not found", id, self);
			return Err(VFSError::InternalError);
		};

		//TODO: actually unmap the user array from the destination VSpace if it's a user VSpace

		if existing_vspace.dec_fdspaces() == 0 {
			vspace_cursor.remove();
		}
		panic!("TODO: fully implement remove_vspace");
	}
}

impl Drop for FDSpace {
	fn drop(&mut self) {
		let fds = unsafe { UnsafeRef::into_box(self.fds_user.clone()) };
		drop(fds);
		self.region.lock().unmap_all().expect("could not unmap FD region for ThreadFDSpace");
	}
}

///Reserves a shared array for FDs
fn reserve_array(max_size: usize, align: usize) -> Result<(usize, usize, Arc<Mutex<VRegionFactory>>), VFSError>{
	let res_size = max_size * align;

	//TODO: only allocate part of the backing
	let region = match VRegionFactory::new(res_size, PAGE_BITS as usize, MemRights::rw(), 0, UtZone::RamAny){
		Ok(f) => f,
		Err(err) => { return Err(VFSError::VSpaceError(err)); },
	};

	//let initial_size = PAGE_SIZE / align;
	let tmp = get_root_vspace();
	let mut vspace = tmp.write();
	match vspace.map(region.clone(), None, None) {
		Ok(addr) => Ok((addr, res_size, region)),
		Err(err) => Err(VFSError::VSpaceError(err)),
	}
}

///Wrapper for `FDSpace` that holds the ID and an RwLock
pub struct FDSpaceContainer {
	id: i32,
	contents: RwLock<FDSpace>,
}

impl FDSpaceContainer {
	///Creates a new FDSpaceContainer
	fn new(contents: FDSpace) -> FDSpaceContainer {
		FDSpaceContainer {
			id: 0,
			contents: RwLock::new(contents),
		}
	}
	///Gets the ID of this VSpace
	pub fn get_id(&self) -> i32 {
		self.id
	}
	///Gets a read-only guard for this VSpace
	pub fn read(&self) -> RwLockReadGuard<FDSpace> {
		self.contents.read()
	}
	///Get a read-write guard for this VSpace
	pub fn write(&self) -> RwLockWriteGuard<FDSpace> {
		self.contents.write()
	}
}

impl ArcListItem for FDSpaceContainer {
	fn get_id(&self) -> i32 {
		self.id
	}
	fn set_id(&mut self, id: i32) {
		self.id = id;
		self.write().id = id;
	}
}

const ROOT_FDSPACE_INITIAL_SIZE: usize = 8192;

///The global list of FDSpaces
pub struct FDSpaceList {
	contents: LockedArcList<FDSpaceContainer>,
	next_id: AtomicI32,
}

impl FDSpaceList {
	///Creates a new `FDSpaceList`.
	fn new() -> FDSpaceList {
		let list = FDSpaceList {
			contents: LockedArcList::new(),
			next_id: AtomicI32::new(-1),
		};
		unsafe { ROOT_FDSPACE = Some(list.new_fdspace_root().expect("failed to initialize process server FDSpace")) };
		unsafe { INITIAL_RESERVED_FDSPACE = Some(RwLock::new(FDSpace::new_root(-1, RESERVED_FDS, RESERVED_FDS).expect("failed to initialize process server FDSpace"))) };
		list
	}
	///Gets an FDSpace from the list
	pub fn get(&self, id: i32) -> Option<Arc<FDSpaceContainer>> {
		if let Some(fd) = self.contents.get(id) {
			Some(fd.clone())
		}else{
			None
		}
	}
	///Creates a new FDSpace for the root server
	pub fn new_fdspace_root(&self) -> Result<Arc<FDSpaceContainer>, VFSError> {
		let fdspace = FDSpace::new_root(0, ROOT_FDSPACE_INITIAL_SIZE, i32::MAX as usize)?;
		let (_, arc_fdspace) = self.contents.add(FDSpaceContainer::new(fdspace))
			.or(Err(VFSError::TooManyFiles))?;

		Ok(arc_fdspace)
	}
	///Removes an FDSpace from the list
	pub fn remove(&self, id: i32) -> Result<(), ()> {
		if self.contents.remove(id).is_ok() {
			Ok(())
		}else{
			Err(())
		}
	}
}

///Wrapper that combines a shareable FDSpace with the internal private one for
///reserved FDs
pub struct ContextFDSpace {
	main: Arc<FDSpaceContainer>,
	reserved: RwLock<FDSpace>,
}

impl ContextFDSpace {
	///Creates a new `ContextFDSpace` for the process server
	pub fn new_root(main: Arc<FDSpaceContainer>, reserved: RwLock<FDSpace>) -> Result<Self, VFSError> {
		Ok(Self {
			main,
			reserved,
		})
	}
	///Locks both internal FDSpaces for reading, returning a guard
	pub fn read<'a>(&'a self) -> ContextFDSpaceReadGuard<'a> {
		ContextFDSpaceReadGuard {
			main: self.main.read(),
			reserved: self.reserved.read(),
		}
	}
	///Locks both internal FDSpaces for writing, returning a guard
	pub fn write<'a>(&'a self) -> ContextFDSpaceWriteGuard<'a> {
		ContextFDSpaceWriteGuard {
			main: self.main.write(),
			reserved: self.reserved.write(),
		}
	}
	///Gets an `Arc` to the shareable FDSpace
	pub fn get_main(&self) -> Arc<FDSpaceContainer> {
		self.main.clone()
	}
}

///Common methods to both read and write `ContextFDSpace` guards
macro_rules! context_fdspace_impls {
	($lt: lifetime) => {
		///Gets the internal state for a file descriptor
		pub fn get_internal(&self, id: RawFileDescriptor) -> Option<&FDInternal> {
			if id < 0 {
				self.reserved.get_internal(get_reserved_index(id))
			}else{
				self.main.get_internal(id)
			}
		}
		///Gets the user-level file descriptor arrays for both shareable and reserved FDSpaces
		pub fn get_user_fds(&self) -> (UnsafeRef<FDArray>, UnsafeRef<FDArray>
	) {
			(self.main.get_user_fds(), self.reserved.get_user_fds())
		}
	}
}

///A read guard for a `ContextFDSpace`
pub struct ContextFDSpaceReadGuard<'a> {
	main: RwLockReadGuard<'a, FDSpace>,
	reserved: RwLockReadGuard<'a, FDSpace>,
}

impl<'a> ContextFDSpaceReadGuard<'a> {
	context_fdspace_impls!('a);
}

///A write guard for a `ContextFDSpace`
pub struct ContextFDSpaceWriteGuard<'a> {
	main: RwLockWriteGuard<'a, FDSpace>,
	reserved: RwLockWriteGuard<'a, FDSpace>,
}

impl<'a> ContextFDSpaceWriteGuard<'a> {
	///Adds a file description to either the shareable or reserved FDSpace
	///depending on the ID specified
	pub fn insert(&mut self, desc: &Arc<FileDescription>, fd_type: FileDescriptorType, access: AccessMode, badge: u32, id: Option<i32>) -> Result<i32, VFSError> {
		if let Some(i) = id && i < 0 {
			self.reserved.insert(desc, fd_type, access, badge, Some(get_reserved_index_replace(i)?))
		}else{
			self.main.insert(desc, fd_type, access, badge, id)
		}
	}
	///Adds a file description to the reserved array, regardless of whether
	///updating the slot is normally permitted
	pub fn insert_reserved(&mut self, desc: &Arc<FileDescription>, fd_type: FileDescriptorType, access: AccessMode, badge: u32, id: i32) -> Result<i32, VFSError> {
		if id < 0 {
			self.reserved.insert(desc, fd_type, access, badge, Some(get_reserved_index_checked(id)?))
		}else{
			Err(VFSError::InvalidArgument)
		}
	}
	///Duplicates a file descriptor (either within the shareable FDSpace or
	///between the shareable and internal FDSpaces)
	pub fn dup(&mut self, src_id: i32, dest_id: Option<i32>, fd_type: Option<FileDescriptorType>, badge: Option<u32>) -> Result<i32, VFSError> {
		if src_id < 0 {
			if let Some(d) = dest_id && d < 0 {
				//currently there are no reserved FD slots that
				//can be dup()ed between
				Err(VFSError::InvalidArgument)
			}else{
				let src_index = get_reserved_index_checked(src_id)?;
				self.reserved.dup_external_locked(src_index, &mut self.main, dest_id, fd_type, badge)
			}
		}else if let Some(d) = dest_id && d < 0 {
			let dest_index = get_reserved_index_replace(d)?;
			self.main.dup_external_locked(src_id, &mut self.reserved, Some(dest_index), fd_type, badge)
		}else{
			self.main.dup(src_id, dest_id, fd_type, badge)
		}
	}
	///Removes a file descriptor from the FDSpace, closing the file
	///description if necessary
	pub fn remove(&mut self, id: i32) -> Result<(), VFSError>{
		if id < 0 {
			Err(VFSError::InvalidArgument)
		}else{
			self.main.remove(id)
		}
	}
	context_fdspace_impls!('a);
}

const RESERVED_FDS: usize = 2;

///Gets the index within the reserved array given a valid negative index
fn get_reserved_index_checked(id: i32) -> Result<i32, VFSError> {
	if id < RESERVED_FDS as i32 {
		Ok(get_reserved_index(id))
	}else{
		Err(VFSError::InvalidArgument)
	}
}

///Gets the index within the reserved array given a valid replaceable negative
///index
fn get_reserved_index_replace(id: i32) -> Result<i32, VFSError> {
	static ALLOWED: [bool; RESERVED_FDS] = [false, true];
	let index = get_reserved_index_checked(id)? as usize;
	if index < RESERVED_FDS && ALLOWED[index] {
		Ok(index as i32)
	}else{
		Err(VFSError::InvalidArgument)
	}
}

pub const ROOT_FDSPACE_ID: i32 = 0;
static mut ROOT_FDSPACE: Option<Arc<FDSpaceContainer>> = None;
static mut INITIAL_RESERVED_FDSPACE: Option<RwLock<FDSpace>> = None;
static mut ROOT_FD_ARRAYS: Option<(UnsafeRef<FDArray>, UnsafeRef<FDArray>)> = None;
static mut FDSPACE_LIST: Option<FDSpaceList> = None;

///Initializes the FDSpace list
pub fn init_fdspace_list() {
	unsafe { FDSPACE_LIST = Some(FDSpaceList::new()) }
}

///Gets the FDSpace list
pub fn get_fdspace_list() -> &'static FDSpaceList {
	unsafe { &FDSPACE_LIST.as_ref().expect("FDSPACE_LIST uninitialized") }
}

///Gets the root FDSpace
pub fn get_initial_reserved_fdspace() -> RwLock<FDSpace> {
	unsafe { INITIAL_RESERVED_FDSPACE.take().expect("reserved FDSpace not initialized") }
}

///Gets the root FDSpace
pub fn get_root_fdspace() -> Arc<FDSpaceContainer> {
	unsafe { ROOT_FDSPACE.as_ref().expect("root FDSpace not initialized").clone() }
}

///Sets the root FD array pointers
pub fn set_root_fd_arrays(arrays: (UnsafeRef<FDArray>, UnsafeRef<FDArray>)){
	unsafe { ROOT_FD_ARRAYS = Some(arrays) }
}

///Gets the FDArray for the root FDSpace
pub fn get_root_fd_arrays() -> (UnsafeRef<FDArray>, UnsafeRef<FDArray>) {
	unsafe { ROOT_FD_ARRAYS.as_ref().expect("root FDSpace not initialized").clone() }
}

///Called to set the thread-local FD arrays for root threads
pub fn fdspace_root_thread_init(main_array: UnsafeRef<FDArray>, reserved_array: UnsafeRef<FDArray>) {
	set_fd_arrays(main_array, reserved_array);
}

///Gets an FD from the current thread's FDSpace
pub fn get_fd(id: i32) -> FileDescriptorRef {
	uxrt_transport_layer::get_fd(id)
}

///Adds FDSpace-related custom slabs
pub fn add_custom_slabs() {
	add_arc_slab!(FDInternal, 512, 4, 4, 2, "file descriptor internal state");
	add_arc_list_slab!(FDSpaceContainer, 512, 4, 4, 2, "FDSpace container");
	add_arc_slab!(FDVSpace, 512, 4, 4, 2, "file descriptor VSpace");
	add_slab::<FDArray>(512, 4, 4, 2, "client state");
}
