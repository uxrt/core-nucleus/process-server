/*
 * Copyright (c) 2024-2025 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */
///This is the VFS RPC handler

use core::cell::{
	Cell,
	RefCell,
};
use core::mem::size_of;

use alloc::vec::Vec;
use alloc::sync::Arc;

use typed_path::{
	UnixPath,
	UnixPathBuf,
};

use usync::RwLock;

use usync_once_cell::OnceCell;

use super::dir::get_dir_handler;

use super::transport::{
	ContextFDSpace,
	FDSpace,
	FDSpaceContainer,
	FileDescription,
	FileDescriptorType,
	get_fd,
	get_initial_reserved_fdspace,
	get_path_list,
	get_root_fdspace,
	set_root_fd_arrays,
};
use super::fsspace::{
	Mount,
	MountTableContainer,
	OpenResult,
	UnmountPath,
	WalkResult,
	get_root_fsspace,
};

use uxrt_transport_layer::{
	AccessMode,
	FileDescriptor,
	FileDescriptorRef,
	IOError,
	MsgHeader,
	OffsetType,
	RawFileDescriptor,
	SRV_ERR_BADF,
	SRV_ERR_INVAL,
	SRV_ERR_IO,
	SRV_ERR_ISDIR,
	SRV_ERR_LOOP,
	SRV_ERR_NOENT,
	SRV_ERR_PROTO,
	ServerStatus,
	UnifiedFileDescriptor,
};

use uxrt_vfs_rpc::{
	AT_FDCWD,
	AT_FDINVALID,
	AT_EMPTY_PATH,
	CWD_FD,
	DEFAULT_RPC_FD,
	O_ACCMODE,
	O_DIRECTORY,
	O_NOFOLLOW,
	O_RDONLY,
	O_TRANSIENT,
	O_USEDEST,
	RESOLVE_EMPTY_PATH,
	RESOLVE_NO_SYMLINKS,
	RESOLVE_NO_DEREF_PARENT_DOTS,
	RPC_MSG_MAX,
	VFS_RPC_SERVER_OPEN_FIRMLINK,
	VFS_RPC_SERVER_OPEN_SYMLINK,
	VFSServerOpenResult,
	access_from_flags,
	dev_t,
	gid_t,
	mode_t,
	open_how,
	stat,
	timespec,
	uid_t,
};

use uxrt_vfs_resmgr::base::{
	DispatchContext,
	DispatchContextFactory,
	RequestBufferArray,
	VFSRPCHandler,
};

use uxrt_vfs_rpc::server::{
	RPC_SECONDARY_MSG_MAX,
	ServerMessageHandler,
};
use uxrt_vfs_rpc_client::VFSRPCClient;

use crate::add_arc_list_slab;

use crate::drivers::portfs::{
	GlobalPortID,
	PortFSMount,
	init_portfs,
	start_portfs,
};


use crate::task::thread_pool::{
	RootThreadPool,
	new_thread_pool,
};

use crate::utils::{
	LockedArcList,
	ArcListItem,
};

use super::{
	new_md,
	new_vfs_server_channel,
};

use super::vnode::get_vnode_list;

use super::VFSError;

pub const PORTFS_MOUNT_ID: i32 = -1;

///Dispatch context for the main VFS RPC handler
struct FSDispatchContext {
	factory: Arc<RPCHandler>,
	message_fd: Option<FileDescriptorRef>,
	transport_header: MsgHeader,
	bufs: RefCell<RequestBufferArray>,
	last_port: Cell<Option<u64>>,
}

impl ServerMessageHandler<()> for FSDispatchContext {
}

impl DispatchContext for FSDispatchContext {
	fn get_transport_header(&self) -> &MsgHeader {
		&self.transport_header
	}
	fn get_transport_header_mut(&mut self) -> &mut MsgHeader {
		&mut self.transport_header
	}
	fn get_channel_fd(&self) -> FileDescriptorRef {
		self.factory.channel_fd
	}
	///Gets the current message FD of this context.
	fn get_message_fd(&self) -> Option<FileDescriptorRef> {
		self.message_fd
	}
	fn new_message_fd(&mut self) -> Result<FileDescriptorRef, IOError> {
		let md = new_md()?;
		self.message_fd = Some(md);
		Ok(md)
	}
	fn handle_message(&mut self) -> Result<(), IOError> {
		let msg_fd = self.get_message_fd().expect("CreatePortContext: message FD unset (this should never happen)");

		let res = self.factory.handle_message(self.get_transport_header(), msg_fd, &mut self.bufs.borrow_mut(), &self);
		self.last_port.set(None);
		res
	}
}

impl Drop for FSDispatchContext {
	fn drop(&mut self) {
		self.drop_common();
	}
}

///Handles VFS RPC messages from clients
pub struct RPCHandler {
	channel_fd: FileDescriptorRef,
	channel_desc: Arc<FileDescription>,
	contexts: LockedArcList<FSContext>,
}

impl RPCHandler {
	///Constructs a new `RPCHandler`
	fn new() -> Result<Arc<Self>, VFSError> {
		let (channel_desc, channel_fd) = new_vfs_server_channel()?;
		Ok(Arc::new(Self {
			channel_fd,
			channel_desc,
			contexts: LockedArcList::new(),
		}))
	}
	///Gets an `FSContext`
	pub fn get_context(&self, id: i32) -> Option<Arc<FSContext>> {
		self.contexts.get(id)
	}
	///Internal method that calls the `FSContext` for a message to handle
	///it (called from an I/O pool thread)
	fn handle_message(&self, transport_header: &MsgHeader, msg_fd: FileDescriptorRef, msg_bufs: &mut RequestBufferArray, dispatch_context: &FSDispatchContext) -> Result<(), IOError> {
		//info!("RPCHandler::handle_message");
		let status = if let Some(context) = self.get_context(transport_header.client_id) {
			if let Err(err) = context.handle_message(transport_header, msg_fd, msg_bufs, dispatch_context) {
				err.to_errno()
			}else{
				return Ok(());
			}
		}else{
			SRV_ERR_BADF
		};
		let _ = msg_fd.mwpwrite(&[], 0, OffsetType::End, status);
		//info!("RPCHandler::handle_message: done");
		Ok(())
	}
	///Creates a new FSContext for a user thread
	fn new_fs_context_user(&self, context_id: i32, fdspace: Arc<FDSpaceContainer>, fsspace_root: Arc<MountTableContainer>) -> Result<Arc<FSContext>, VFSError> {
		//TODO: set the FDSpace ID of the internal reserved FDSpace to -(context_id + 1) since negative FDSpace IDs aren't otherwise used
		unimplemented!()
	}
	///Creates a new FSContext for a process server thread
	fn new_fs_context_root(self: &Arc<Self>, context_id: i32, fdspace: Arc<FDSpaceContainer>, reserved_fdspace: RwLock<FDSpace>, fsspace_root: Arc<MountTableContainer>) -> Result<Arc<FSContext>, VFSError> {
		let context_fdspace = ContextFDSpace::new_root(fdspace, reserved_fdspace)?;
		self.new_fs_context_common(context_id, context_fdspace, fsspace_root)
	}
	///Internal base constructor for filesystem contexts
	fn new_fs_context_common(self: &Arc<Self>, context_id: i32, fdspace: ContextFDSpace, fsspace_root: Arc<MountTableContainer>) -> Result<Arc<FSContext>, VFSError> {
		let rpc_connection = self.get_file_description().new_shared_client(0, Some(context_id), None)?;
		let context = FSContext {
			rpc_connection: rpc_connection.clone(),
			fdspace,
			fsspace_root,
			ports: Default::default(),
			context_id,
		};
		let (_, arc) = self.contexts.add(context).or(Err(VFSError::InternalError))?;
		let ports = PortFSMount::new_main(&arc)?;
		arc.ports.set(ports).expect("portfs already set (this should never happen!");

		let mut f = arc.fdspace.write();
		f.insert_reserved(&rpc_connection, FileDescriptorType::Client, AccessMode::ReadWrite, 0, DEFAULT_RPC_FD)?;
		Ok(arc.clone())
	}
	///Gets the file description for this message handler
	pub fn get_file_description(&self) -> &Arc<FileDescription> {
		&self.channel_desc
	}
}

impl DispatchContextFactory<FSDispatchContext> for Arc<RPCHandler> {
	///Creates a new `ResMgrContext` attached to this port.
	fn new_context(&self) -> Result<FSDispatchContext, IOError> {
		let context = FSDispatchContext {
			factory: self.clone(),
			message_fd: None,
			transport_header: MsgHeader::new(),
			bufs: Default::default(),
			last_port: Default::default(),
		};
		let mut bufs = context.bufs.borrow_mut();
		bufs.push(Vec::with_capacity(RPC_MSG_MAX as usize));
		bufs.push(Vec::with_capacity(RPC_SECONDARY_MSG_MAX));
		drop(bufs);
		Ok(context)
	}
}

const MAX_NESTED_PATHS: usize = 8;

///Returned from connect handler functions to either stop resolution or restart
///it with a different path
pub enum ConnectResult {
	Restart(VFSServerOpenResult, Vec<u8>, Arc<FSContext>),
	Continue,
	Finish,
}

///Returned from normalize_path to allow using the original string in place if
///no normalization was required
pub enum NormalizedPath<'a> {
	Orig(&'a UnixPath),
	Copied(UnixPathBuf),
}

impl<'a> NormalizedPath<'a> {
	///Converts this to a PathBuf, creating a new PathBuf if necessary
	fn to_path_buf(self) -> UnixPathBuf {
		match self {
			NormalizedPath::Orig(ref path) => UnixPathBuf::from(path),
			NormalizedPath::Copied(buf) => buf,
		}
	}
	///Gets a path slice reference to the contents
	fn as_path_slice(&'a self) -> &'a UnixPath {
		match self {
			NormalizedPath::Orig(ref path) => path,
			NormalizedPath::Copied(ref buf) => buf.as_path(),
		}
	}
}

///Returns true iff the given depth of the path is a leaf node rather than a
///directory
fn link_is_leaf(suffix: &UnixPath, depth: u64) -> bool {
	let mut iter = suffix.components();
	let mut max_index = 0;
	while iter.next().is_some() {
		max_index += 1;
		if max_index > depth {
			return false
		}
	}
	depth == max_index
}

//TODO: if a context has ports still open from other contexts and/or has a directory FD open to it, delete it only once the last port and last directory FD have been closed

///A filesystem context. This holds all of the VFS-related state for a thread
///or group of threads and ties together all of the other parts of the VFS.
pub struct FSContext {
	rpc_connection: Arc<FileDescription>,
	fdspace: ContextFDSpace,
	fsspace_root: Arc<MountTableContainer>,
	ports: OnceCell<Arc<PortFSMount>>,
	context_id: i32,
}

impl FSContext {
	///Gets the ID of this context
	pub fn get_id(&self) -> i32 {
		self.context_id
	}
	///Gets the base `FDSpace` associated with this context, excluding
	///reserved FDs
	pub fn get_fdspace(&self) -> Arc<FDSpaceContainer> {
		self.fdspace.get_main()
	}
	///Gets the `FDSpace` associated with this context, including reserved
	///FDs
	pub fn get_context_fdspace(&self) -> &ContextFDSpace {
		&self.fdspace
	}
	///Gets the root `MountTable` associated with this context
	pub fn get_fsspace_root(&self) -> Arc<MountTableContainer> {
		self.fsspace_root.clone()
	}
	///Gets the port filesystem of this context
	pub fn get_ports(&self) -> Arc<PortFSMount> {
		self.ports.get().expect("portfs unset for context (this should never happen!)").clone()
	}
	///Gets the file descriptor struct and client ID to use when sending
	///I/O RPC requests for a file descriptor in this context's FDSpace
	fn get_rpc_io_connection(&self, fd: i32) -> Result<(VFSRPCClient<UnifiedFileDescriptor>, i32), ServerStatus> {
		let f = self.get_fdspace();
		let fdspace = f.read();
		let fd_internal = fdspace.get_internal(fd).ok_or(SRV_ERR_BADF)?;
		let (io_fd, conn_id) = fd_internal.get_rpc_io_fd()?;
		Ok((VFSRPCClient::new(io_fd), conn_id))
	}
	///Handles an RPC message
	fn handle_message(self: &Arc<Self>, transport_header: &MsgHeader, msg_fd: FileDescriptorRef, request_bufs: &mut RequestBufferArray, dispatch_context: &FSDispatchContext) -> Result<(), IOError>  {
		self.handle_vfs_rpc(transport_header, msg_fd, request_bufs, &(&self, dispatch_context))
	}
	///Normalizes this path for resolution
	fn normalize_path<'a>(&self, mut open_flags: u64, resolve_flags: u64, at_path: Option<&UnixPath>, path: &'a UnixPath) -> Result<(NormalizedPath<'a>, u64, u64), ServerStatus> {
		//TODO: escape control characters (including CR and LF)
		//TODO: make sure the path is truncated at the first null

		if resolve_flags & RESOLVE_NO_DEREF_PARENT_DOTS as u64 == 0 {
			panic!("TODO: implement dereferencing for parent components of dots");
		}

		if resolve_flags & RESOLVE_EMPTY_PATH as u64 == 0 && path.as_bytes().len() == 0 {
			return Err(SRV_ERR_NOENT);
		}

		let abs_path = if path.is_relative() {
			if let Some(p) = at_path {
				//info!("normalize_path: {:?} {:?}", core::str::from_utf8(p.as_bytes()), core::str::from_utf8(path.as_bytes()));
				NormalizedPath::Copied(p.join(path))
			}else{
				return Err(SRV_ERR_NOENT);
			}
		}else{
			NormalizedPath::Orig(path)
		};
		let mut filtered_len = 0;
		let mut components = Vec::new();
		for component in abs_path.as_path_slice().iter(){
			let mut non_dot_found = false;
			for c in component {
				if *c != '.' as u8 {
					non_dot_found = true;
					break;
				}
			}
			if non_dot_found {
				filtered_len += component.len() + 1;
				components.push(component);
				continue;
			}
			let levels = component.len() - 1;

			for _ in 0..levels {
				if components.len() > 1 && components.pop().is_none() {
					break;
				}
			}
		}

		//anything ending in / or /. gets O_DIRECTORY set regardless
		//of whether the client requested it
		if path.as_bytes().ends_with("/".as_bytes()) ||
			path.as_bytes().ends_with("/.".as_bytes()) {
				open_flags |= O_DIRECTORY as u64;
		}

		if filtered_len > 0 && filtered_len - 2 == abs_path.as_path_slice().as_bytes().len() {
			//info!("normalize_path: using original path: {:?}", core::str::from_utf8(abs_path.as_path_slice().as_bytes()));
			Ok((abs_path, open_flags, 0))
		}else{
			let mut filtered_path = UnixPathBuf::new();
			for component in components {
				filtered_path.push(component);
			}
			//info!("normalize_path: using filtered path: {:?}", core::str::from_utf8(filtered_path.as_path().as_bytes()));
			Ok((NormalizedPath::Copied(filtered_path), open_flags, 0))
		}
	}
	///Common implementation for all connect-type RPC functions
	pub fn connect_path<F>(&self,
			dirfd: RawFileDescriptor,
			path: &[u8],
			open_flags: u64,
			resolve_flags: u64,
			fallthrough_status: ServerStatus,
			f: &mut F) -> Result<(UnixPathBuf, UnixPathBuf, UnixPathBuf), ServerStatus>
			where F: FnMut(&UnixPath, &Arc<dyn Mount + Send + Sync>, u64, u64) -> Result<ConnectResult, ServerStatus> {
		if dirfd == AT_FDCWD && path.len() == 0 {
			return Err(SRV_ERR_NOENT);
		}
		let (orig_path_opt, real_path, mount_path) = self.connect_path_inner(0, dirfd, &UnixPath::new(path), &UnixPath::new(""), open_flags, resolve_flags, fallthrough_status, f)?;
		Ok((orig_path_opt.unwrap(), real_path, mount_path))
	}
	///Internal implementation of connect_path, which calls itself
	///recursively to deal with symlinks and redirects
	fn connect_path_inner<F>(&self,
			depth: usize,
			mut dirfd: RawFileDescriptor,
			cur_path: &UnixPath,
			prev_path: &UnixPath,
			mut open_flags: u64,
			mut resolve_flags: u64,
			fallthrough_status: ServerStatus,
			f: &mut F) -> Result<(Option<UnixPathBuf>, UnixPathBuf, UnixPathBuf), ServerStatus>
			where F: FnMut(&UnixPath, &Arc<dyn Mount + Send + Sync>, u64, u64) -> Result<ConnectResult, ServerStatus> {
		//TODO: handle magic ..:<feature>/ suffixes for things like accessing forks and individual layers
		//info!("FSContext::connect_path_inner: {:?}", core::str::from_utf8(cur_path.as_bytes()));

		if dirfd == AT_FDCWD {
			dirfd = CWD_FD;
		}

		let at_desc;
		let at_path_guard;
		let at_path = if depth > 0 {
			//this is a link target, so resolve it relative to the
			//directory that contains the link if it is relative
			if let Some(p) = prev_path.parent() {
				Some(p)
			}else{
				None
			}
		}else if dirfd == AT_FDINVALID || cur_path.is_absolute() {
			//info!("FSContext::connect_path_inner: dirfd invalid or absolute path: {} {}", dirfd, cur_path.is_absolute());
			None
		}else{
			let fdspace = self.fdspace.read();
			//info!("FSContext::connect_path_inner: resolving relative path with FD {}", dirfd);
			let fd_opt = fdspace.get_internal(dirfd);
			drop(fd_opt);
			if let Some(fd) = fd_opt {
				//info!("dirfd found");
				at_desc = fd.get_file_description();
				at_path_guard = at_desc.get_real_path();
				if let Some(p) = at_path_guard.as_ref() && at_desc.is_base_path() {
					Some(p.as_path())
				}else{
					//info!("dirfd not a directory");
					None
				}
			}else{
				//info!("dirfd not found");
				None
			}
		};
		let (normalized_path, extra_open_flags, extra_resolve_flags) = self.normalize_path(open_flags, resolve_flags, at_path, cur_path)?;
		open_flags |= extra_open_flags;
		resolve_flags |= extra_resolve_flags;
		//info!("FSContext::connect_path_inner: normalized path: {:?} {:?}", core::str::from_utf8(normalized_path.as_path_slice().as_bytes()), core::str::from_utf8(prev_path.as_bytes()));
		let fallthrough_res = if fallthrough_status == 0 {
			Ok(())
		}else{
			Err(fallthrough_status)
		};
		let mut walk_res = Err(SRV_ERR_NOENT);
		let mut real_path_opt = None;
		let mut mount_path_opt = None;
		self.fsspace_root.read().walk(fallthrough_res, normalized_path.as_path_slice(), |suffix, binding| {
			//info!("FSContext::connect_path_inner: mount path: {:?}", core::str::from_utf8(suffix.as_bytes()));
			match f(suffix, binding, open_flags, resolve_flags) {
				Ok(res) => match res {
					ConnectResult::Restart(open_result, new_path_raw, context) => 'restart: {
						//info!("connect_path: restarting: {:?}", core::str::from_utf8(&new_path_raw));
						if depth > MAX_NESTED_PATHS {
							break 'restart Err(SRV_ERR_LOOP);
						}
						let new_path_base = UnixPath::new(&new_path_raw);
						if open_result.res_type == VFS_RPC_SERVER_OPEN_FIRMLINK as u64 {
							if Arc::as_ptr(&context) != self {
								panic!("TODO: resolve firmlinks from other contexts");
							}
							if new_path_base.is_relative() {
								//TODO: interpret the high 32 bits of the result value as a directory/path FD in the server's context and resolve paths relative to that, in order to allow firmlinks to be used in a capability-oriented manner
								break 'restart Err(SRV_ERR_NOENT);
							}
						}else if open_result.res_type == VFS_RPC_SERVER_OPEN_SYMLINK as u64 {
							if resolve_flags as u32 & RESOLVE_NO_SYMLINKS != 0 ||
								(open_flags as u32 & O_NOFOLLOW != 0
							    && link_is_leaf(suffix, open_result.res_value)) {
								break 'restart Err(SRV_ERR_LOOP);
							}
						}else{
							break 'restart Err(SRV_ERR_PROTO);
						}
						let mut iter = suffix.components();
						//strip off the parts of the
						//path up to and including the
						//link
						for _ in 0..open_result.res_value {
							if iter.next().is_none() {
								break 'restart Err(SRV_ERR_NOENT);
							}
						}
						let new_path = new_path_base.join(iter.as_path());
						//info!("FSContext::connect_path_inner: {:?} {:?}", core::str::from_utf8(new_path.as_bytes()), core::str::from_utf8(normalized_path.as_bytes()));
						let (_, r, m) = self.connect_path_inner(depth + 1, AT_FDINVALID, UnixPath::new(&new_path), normalized_path.as_path_slice(), open_flags, resolve_flags, fallthrough_status, f)?;
						real_path_opt = Some(r);
						mount_path_opt = Some(m);
						walk_res = Ok(());
						Ok(WalkResult::Finish)
					},
					ConnectResult::Continue => {
						if mount_path_opt.is_none(){
							mount_path_opt = Some(UnixPathBuf::from(suffix));
						}
						walk_res = Ok(());
						Ok(WalkResult::Continue)
					},
					ConnectResult::Finish => {
						//info!("connect_path: walk finished");
						walk_res = Ok(());
						mount_path_opt = Some(UnixPathBuf::from(suffix));
						Ok(WalkResult::Finish)
					},
				},
				Err(errno) => {
					if errno == SRV_ERR_NOENT {
						Ok(WalkResult::Continue)
					}else{
						Err(errno)
					}
				},
			}
		})?;
		walk_res?;
		let orig_path = if depth == 0 {
			Some(normalized_path.as_path_slice().to_path_buf())
		}else{
			None
		};
		let real_path = if let Some(p) = real_path_opt {
			p
		}else if let Some(ref path) = orig_path {
			path.clone()
		}else{
			normalized_path.as_path_slice().to_path_buf()
		};
		Ok((orig_path, real_path, mount_path_opt.ok_or(SRV_ERR_IO)?))
	}
	///Internal implementation of openat2() for non-directories
	fn openat2_file(&self, transport_header: &MsgHeader, dirfd: RawFileDescriptor, path: &[u8], how: &open_how, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize], contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(UnixPathBuf, UnixPathBuf, UnixPathBuf, Option<Arc<FileDescription>>, FileDescriptorType, u32, u64, u64, AccessMode, Option<Arc<dyn Mount + Send + Sync>>, bool), ServerStatus> {
		//info!("FSContext::openat2_file: {:?}", core::str::from_utf8(path));
		let access = access_from_flags(how.flags)?;
		let mut res_opt = None;
		let mut how_clone = how.clone();
		let upper_res = self.connect_path(dirfd, path, how.flags, how.resolve, SRV_ERR_NOENT, &mut |suffix, mount, open_flags, resolve_flags| {
			how_clone.flags |= open_flags;
			how_clone.resolve |= resolve_flags;
			match mount.handle_openat2(transport_header, self, suffix, &how_clone, secondary_bufs, secondary_len)? {
				OpenResult::Restart(res, new_path, context) => Ok(ConnectResult::Restart(res, new_path, context)),
				OpenResult::Finish(desc, fd_type, badge, port_id, inode, secondary_size) => {
					if secondary_size.is_some() {
						panic!("TODO: implement secondary request handling");
					}
					res_opt = Some((desc, fd_type, badge, port_id, inode, Some(mount.clone())));

					Ok(ConnectResult::Finish)
				},
				OpenResult::Abort => {
					res_opt = None;
					Ok(ConnectResult::Finish)
				},
			}
		});
		if upper_res == Err(SRV_ERR_ISDIR){
			//try to open the path as a directory instead (since
			//some programs may expect to open directories without
			//O_DIRECTORY or O_PATH)
			return self.openat2_dir(transport_header, dirfd, path, how, secondary_bufs, secondary_len, contexts);
		}
		let (orig_path, real_path, mount_path) = upper_res?;
		if let Some((desc, fd_type, badge, port, inode, mount_opt)) = res_opt {
			Ok((
				orig_path,
				real_path,
				mount_path,
				desc,
				fd_type,
				badge,
				port,
				inode,
				access,
				mount_opt,
				false
			))
		}else{
			Err(SRV_ERR_NOENT)
		}
	}
	///Internal implementation of openat2() for directories
	fn openat2_dir(&self, transport_header: &MsgHeader, dirfd: RawFileDescriptor, path: &[u8], how: &open_how, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize], contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(UnixPathBuf, UnixPathBuf, UnixPathBuf, Option<Arc<FileDescription>>, FileDescriptorType, u32, u64, u64, AccessMode, Option<Arc<dyn Mount + Send + Sync>>, bool), ServerStatus> {
		//info!("FSContext::openat2_dir: {:?}", core::str::from_utf8(path));
		let dir_handler = get_dir_handler();
		let ocb = dir_handler.new_client(contexts.0, path, dirfd, how)?;
		let desc = ocb.get_file_description();
		let mut res = ocb.open(transport_header, secondary_bufs, secondary_len);

		if how.flags & O_ACCMODE as u64 != O_RDONLY as u64 && res.is_ok() {
			res = Err(SRV_ERR_ISDIR);
		}

		let mut abort = false;
		let ret = match res {
			Ok((orig_path, real_path, mount_path, port, inode)) => {
				let desc_ret = if how.flags & O_TRANSIENT as u64 != 0 {
					abort = true;
					None
				} else {
					Some(desc.clone())
				};
				Ok((orig_path, real_path, mount_path, desc_ret, FileDescriptorType::Client, 0, port, inode, AccessMode::ReadWrite, None, true))
			},
			Err(err) => {
				abort = true;
				Err(err)
			}
		};
		if abort {
			//info!("openat2_dir: aborting connection");
			dir_handler.delete_client(desc.get_connection_id());
			desc.open_aborted(FileDescriptorType::Client)?;
		};
		ret
	}
	///Gets the mount manager for the specified mount port ID (if the port
	///specified doesn't exist or is not a mount channel,
	///VFSError::FileNotFound is returned)
	pub fn get_mount(&self, id: RawFileDescriptor) -> Result<Arc<dyn Mount + Send + Sync>, VFSError> {
		if id == PORTFS_MOUNT_ID {
			Ok(self.get_ports())
		}else{
			self.get_ports().get_resmgr(id)
		}
	}
	///Mounts a filesystem in the FSSpace of this context
	pub fn mount(&self, index: isize, src: &UnixPath, dest: &UnixPath) -> Result<Arc<dyn Mount>, VFSError> {
		//TODO: normalize the path
		self.fsspace_root.write().mount(index, src, dest)
	}
	///Unmounts a filesystem from the FSSpace of this context
	pub fn unmount(&self, index: Option<isize>, path: UnmountPath) -> Result<(), VFSError>{
		unimplemented!()
	}
}

impl ArcListItem for FSContext {
	fn get_id(&self) -> i32 {
		self.context_id
	}
	fn set_id(&mut self, id: i32) {
		self.context_id = id;
	}
}

impl VFSRPCHandler<(&Arc<FSContext>, &FSDispatchContext)> for FSContext {
}

const DEST_SHR: u32 = (size_of::<i32>() * 8) as u32;

impl ServerMessageHandler<(&Arc<FSContext>, &FSDispatchContext)> for FSContext {
	fn handle_openat2(&self, transport_header: &MsgHeader, dirfd: RawFileDescriptor, path: &[u8], how_orig: &open_how, secondary_bufs: &mut [Vec<u8>], secondary_len: &mut [usize], contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(RawFileDescriptor, bool), ServerStatus> {
		//info!("FSContext::handle_openat2: {:?}", core::str::from_utf8(path));
		let mut how = how_orig.clone();
		//TODO: allow configuring this on a per-context basis
		how.resolve |= RESOLVE_NO_DEREF_PARENT_DOTS as u64;
		let (orig_path, real_path, mount_path, desc_opt, fd_type, badge, port, inode, access, mount_opt, is_dir) = if how.flags & O_DIRECTORY as u64 != 0 {
			self.openat2_dir(transport_header, dirfd, path, &how, secondary_bufs, secondary_len, contexts)?
		}else{
			self.openat2_file(transport_header, dirfd, path, &how, secondary_bufs, secondary_len, contexts)?
		};
		contexts.1.last_port.set(Some(port));
		let desc = if let Some(d) = desc_opt {
			d
		}else{
			return Ok((0, true));
		};
		desc.set_paths(&orig_path, &real_path);
		if inode != 0 {
			if desc.get_global_id() != GlobalPortID::invalid().value() {
				let mut vnode_list = get_vnode_list().write();
				vnode_list.add_desc(&desc, port, inode)?;
			}else if !is_dir {
				return Err(SRV_ERR_IO);
			}
			let mut path_list = get_path_list().write();
			path_list.add_desc(&desc, &mount_path);
		}
		let dest = if how.flags & O_USEDEST as u64 != 0 {
			Some(how.mode.wrapping_shr(DEST_SHR) as i32)
		}else{
			None
		};
		//info!("FSContext::handle_openat2: specified dest ID: {:?}", dest);

		let mut fdspace = self.fdspace.write();
		let insert_res = fdspace.insert(&desc, fd_type, access, badge, dest);
		drop(fdspace);
		match insert_res {
			Ok(id) => {
				//info!("FSContext::handle_openat2: opened FD with ID {}", id);
				Ok((id, true))
			},
			Err(err) => {
				//info!("FSContext::handle_openat2: open failed with {:?}", err);
				if let Some(mount) = mount_opt {
					mount.handle_open_fail(&desc, fd_type, badge);
				}
				Err(err.to_errno())
			},
		}
	}
	fn handle_openfd(&self, _transport_header: &MsgHeader, fd: RawFileDescriptor, flags: i32, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<RawFileDescriptor, ServerStatus> {
		info!("TODO: openfd: {} {:x}", fd, flags);
		Ok(0xabcd)
	}
	fn handle_dup3(&self, _transport_header: &MsgHeader, oldfd: RawFileDescriptor, newfd: RawFileDescriptor, flags: i32, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<RawFileDescriptor, ServerStatus> {
		if flags != 0 {
			panic!("TODO: implement flags for dup3");
		}
		let mut fdspace = self.fdspace.write();
		let newfd_opt = if newfd == AT_FDINVALID {
			None
		}else{
			Some(newfd)
		};
		let f = fdspace.dup(oldfd, newfd_opt, None, None)?;
		Ok(f)
	}
	fn handle_close(&self, _transport_header: &MsgHeader, fd: RawFileDescriptor, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		//info!("FSContext::handle_close: {}", fd);
		let mut fdspace = self.fdspace.write();
		fdspace.remove(fd)?;
		Ok(())
	}
	fn handle_fcntl(&self, _transport_header: &MsgHeader, fd: RawFileDescriptor, cmd: i32, _arg: &[u8], ret: &mut [u8], _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<usize, ServerStatus> {
		info!("TODO: fcntl: {} {}", fd, cmd);
		ret[0] = 0xab;
		Ok(1)
	}
	fn handle_fstat(&self, transport_header: &MsgHeader, fd: RawFileDescriptor, statbuf: &mut stat, contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		let (rpc, conn) = self.get_rpc_io_connection(fd)?;
		rpc.fstat(conn, statbuf)?;
		self.check_fstat(transport_header, fd, statbuf, contexts)?;
		Ok(())
	}
	fn check_fstat(&self, _transport_header: &MsgHeader, fd: RawFileDescriptor, statbuf: &mut stat, contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		statbuf.st_dev = if let Some(port) = contexts.1.last_port.get() {
			//info!("FSContext::check_fstat: using port from combined open");
			port
		}else{
			//info!("FSContext::check_fstat: looking up port from FDSpace");
			let f = self.get_fdspace();
			let fdspace = f.read();
			let fd_internal = fdspace.get_internal(fd).ok_or(SRV_ERR_BADF)?;
			let desc = fd_internal.get_file_description();
			if let Some(vnode) = desc.get_vnode() {
				vnode.get_port_id()
			}else{
				desc.get_global_id()
			}
		};
		//info!("FSContext::check_fstat: port: {}", statbuf.st_dev);
		Ok(())
	}
	fn handle_readlinkat2(&self, transport_header: &MsgHeader, dirfd: RawFileDescriptor, path: &mut [u8], path_size: usize, res_offset: usize, flags: i32, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<usize, ServerStatus>{
		//info!("FSContext::handle_readlinkat2");
		let mut how = open_how {
			flags: (O_TRANSIENT | O_RDONLY) as u64,
			mode: 0,
			resolve: RESOLVE_NO_DEREF_PARENT_DOTS as u64,
		};
		if flags & AT_EMPTY_PATH as i32 != 0 {
			how.flags |= RESOLVE_EMPTY_PATH as u64;
		}
		let mut target_opt = None;
		let mut connect_res = self.connect_path(dirfd, &path[..path_size], how.flags, how.resolve, SRV_ERR_NOENT, &mut |suffix, mount, open_flags, resolve_flags| {
			//info!("handle_readlinkat: {:?}", core::str::from_utf8(suffix.as_bytes()));
			how.flags = open_flags;
			how.resolve = resolve_flags;
			match mount.handle_openat2(transport_header, self, suffix, &how, &mut [], &mut [])? {
				OpenResult::Restart(res, new_path, context) => {
					if link_is_leaf(suffix, res.res_value) {
						//info!("returning link target");
						target_opt = Some(new_path);
						Ok(ConnectResult::Finish)
					}else{
						//info!("dereferencing link: {} {}", res.res_value, max_index);
						Ok(ConnectResult::Restart(res, new_path, context))
					}
				},
				OpenResult::Finish(desc_opt, _, _, _, _, _) => {
					if let Some(desc) = desc_opt {
						desc.open_aborted(FileDescriptorType::Client)?;
					}
					Err(SRV_ERR_INVAL)
				},
				OpenResult::Abort => Err(SRV_ERR_NOENT),
			}
		});
		if connect_res == Err(SRV_ERR_ISDIR) {
			connect_res = Err(SRV_ERR_INVAL);
		}
		//info!("handle_readlinkat: {:?}", connect_res);
		connect_res?;
		if let Some(target) = target_opt {
			path[res_offset..res_offset + target.len()].copy_from_slice(&target);
			Ok(target.len())
		}else{
			Err(SRV_ERR_NOENT)
		}
	}
	fn handle_fchmod(&self, _transport_header: &MsgHeader, fd: RawFileDescriptor, mode: mode_t, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		let (rpc, conn) = self.get_rpc_io_connection(fd)?;
		rpc.fchmod(conn, mode)?;
		Ok(())
	}
	fn handle_fchown(&self, _transport_header: &MsgHeader, fd: RawFileDescriptor, uid: uid_t, gid: gid_t, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		let (rpc, conn) = self.get_rpc_io_connection(fd)?;
		rpc.fchown(conn, uid, gid)?;
		Ok(())
	}
	fn handle_futimens(&self, _transport_header: &MsgHeader, fd: RawFileDescriptor, times: &[timespec; 2], _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		info!("TODO: utimensat: {} {} {} {} {}", fd, times[0].tv_sec, times[0].tv_nsec, times[1].tv_sec, times[1].tv_nsec);
		Ok(())
	}
	fn handle_linkat(&self, _transport_header: &MsgHeader, olddirfd: RawFileDescriptor, oldpath: &[u8], newdirfd: RawFileDescriptor, newpath: &[u8], flags: i32, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		let oldpath_utf8 = core::str::from_utf8(oldpath).expect("non-UTF-8 sequence in old path");
		let newpath_utf8 = core::str::from_utf8(newpath).expect("non-UTF-8 sequence in new path");

		info!("TODO: linkat: {} {} {} {} {:x}", olddirfd, oldpath_utf8, newdirfd, newpath_utf8, flags);
		Ok(())
	}
	fn handle_symlinkat2(&self, _transport_header: &MsgHeader, target: &[u8], newdirfd: RawFileDescriptor, linkpath: &[u8], flags: i32, contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		let target_utf8 = core::str::from_utf8(target).expect("non-UTF-8 sequence in old path");
		let linkpath_utf8 = core::str::from_utf8(linkpath).expect("non-UTF-8 sequence in new path");

		info!("TODO: symlinkat: {} {} {} {}", target_utf8, newdirfd, linkpath_utf8, flags);
		Ok(())
	}
	fn handle_mknodat2(&self, _transport_header: &MsgHeader, dirfd: RawFileDescriptor, path: &[u8], mode: mode_t, dev: dev_t, flags: i32, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		let path_utf8 = core::str::from_utf8(path).expect("non-UTF-8 sequence in path");
		info!("TODO: mknodat: {} {} {} {} {}", dirfd, path_utf8, mode, dev, flags);
		Ok(())
	}
	fn handle_renameat2(&self, _transport_header: &MsgHeader, olddirfd: RawFileDescriptor, oldpath: &[u8], newdirfd: RawFileDescriptor, newpath: &[u8], flags: i32, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		let oldpath_utf8 = core::str::from_utf8(oldpath).expect("non-UTF-8 sequence in old path");
		let newpath_utf8 = core::str::from_utf8(newpath).expect("non-UTF-8 sequence in new path");

		info!("TODO: renameat2: {} {} {} {} {:x}", olddirfd, oldpath_utf8, newdirfd, newpath_utf8, flags);
		Ok(())
	}
	fn handle_unlinkat(&self, transport_header: &MsgHeader, dirfd: RawFileDescriptor, pathname: &[u8], flags: i32, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		//TODO: handle flags
		self.connect_path(dirfd, pathname, 0, RESOLVE_NO_DEREF_PARENT_DOTS as u64, SRV_ERR_NOENT, &mut |suffix, binding, open_flags, resolve_flags| {
			binding.handle_unlinkat(transport_header, self, suffix, flags)
		})?;
		Ok(())
	}
	fn handle_getpages(&self, _transport_header: &MsgHeader, fd: RawFileDescriptor, start: usize, size: usize, _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<usize, ServerStatus> {
		info!("TODO: getpages: {} {} {}", fd, start, size);
		Ok(0xabcd)
	}
	fn handle_mount(&self, _transport_header: &MsgHeader, mount_id: i32, dev: &[u8], fstype: &[u8], opts: &[u8], _contexts: &(&Arc<Self>, &FSDispatchContext)) -> Result<(), ServerStatus> {
		let dev_utf8 = core::str::from_utf8(dev).expect("non-UTF-8 sequence in old path");
		let fstype_utf8 = core::str::from_utf8(fstype).expect("non-UTF-8 sequence in new path");
		let opts_utf8 = core::str::from_utf8(opts).expect("non-UTF-8 sequence in new path");

		info!("TODO: mount: {} {} {} {}", mount_id, dev_utf8, fstype_utf8, opts_utf8);
		Ok(())
	}
}

static RPC_THREAD_POOL: OnceCell<Arc<RootThreadPool<Arc<RPCHandler>, FSDispatchContext>>> = OnceCell::new();
static ROOT_DEFAULT_FS_CONTEXT: OnceCell<Arc<FSContext>> = OnceCell::new();

///Initializes the VFS RPC layer, starting the server and initializing the
///default client connection for the process server
pub fn init_rpc_handler(){
	info!("initializing RPC handler");
	let orig_handler = RPCHandler::new().expect("cannot create VFS RPC handler");
	let thread_pool = RPC_THREAD_POOL.get_or_init(|| {
		new_thread_pool::<Arc<RPCHandler>, FSDispatchContext>(orig_handler, 2, 10, i32::MAX as usize, 2, "vfs_rpc_handler").expect("cannot create PortFS thread pool")
	});

	let handler = thread_pool.get_factory();
	let fdspace = get_root_fdspace();
	let fsspace = get_root_fsspace();
	init_portfs();
	let reserved_fdspace = get_initial_reserved_fdspace();
	let context = handler.new_fs_context_root(0, fdspace, reserved_fdspace, fsspace.clone()).expect("cannot create initial RPC client");
	let context_fdspace = context.fdspace.read();
	set_root_fd_arrays(context_fdspace.get_user_fds());

	ROOT_DEFAULT_FS_CONTEXT.get_or_init(|| {
		context.clone()
	});

}

///Starts the main thread of the RPC handler
pub fn start_rpc_handler(){
	info!("starting VFS RPC handler");
	RPC_THREAD_POOL.get().expect("RPC handler uninitialized").start().expect("failed to start VFS RPC handler");
	start_portfs();
}

///Gets the default `FSContext` for the root server
pub fn get_root_default_fs_context() -> Arc<FSContext> {
	ROOT_DEFAULT_FS_CONTEXT.get().expect("default root FS context unset").clone()
}

///Gets the running RPC handler
pub fn get_rpc_handler() -> Arc<RPCHandler> {
	let pool = RPC_THREAD_POOL.get().expect("RPC handler unset");
	pool.get_factory().clone()
}

///Creates a new RPC client file description for the running RPC handler
pub fn new_fs_context(id: i32, fdspace: Arc<FDSpaceContainer>, fsspace: Arc<MountTableContainer>) -> Result<Arc<FSContext>, VFSError> {
	unimplemented!()
	//get_rpc_handler().new_fs_context_root(id, fdspace, fsspace)
}

///Gets an FSContext
pub fn get_fs_context(id: i32) -> Option<Arc<FSContext>> {
	get_rpc_handler().get_context(id)
}

///Gets the default RPC client connection
pub fn get_default_rpc_client() -> VFSRPCClient<FileDescriptorRef> {
	VFSRPCClient::new(get_fd(-1))
}

///Dumps the list of ports associated with the root FSContext
pub fn dump_ports(){
	info!("Ports:");
	info!("{:?}", get_root_default_fs_context().get_ports());
}

///Adds RPC-related custom slabs
pub fn add_custom_slabs() {
	add_arc_list_slab!(FSContext, 128, 4, 4, 2, "FSContext");
}
