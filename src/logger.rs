/*
 * Copyright (c) 2018-2024 Andrew Warkentin
 *
 * This software may be distributed and modified according to the terms of
 * the GNU General Public License version 2 or (at your option) any later
 * version. Note that NO WARRANTY is provided. See "LICENSE-GPLv2" for
 * details.
 *
 */

use core::cell::{
	Cell,
	RefCell,
};
use core::sync::atomic::{
	AtomicBool,
	AtomicI32,
	Ordering
};
use core::fmt;
use alloc::string::String;
use log::{Record, Level, Metadata, SetLoggerError, LevelFilter};
use sel4::PAGE_SIZE;
use uxrt_transport_layer::{
	FileDescriptor,
	FileDescriptorRef,
	get_fd,
};
use uxrt_vfs_rpc::O_RDWR;
use crate::task::thread::{
	get_current_name,
	get_current_tid,
	set_current_id_and_name,
};
use crate::task::task_state_initialized;
use crate::vfs::rpc::get_default_rpc_client;

static USE_DEV_DMESG_DEFAULT: AtomicBool = AtomicBool::new(false);
#[thread_local]
static USE_DEV_DMESG_THREAD: Cell<Option<bool>> = Cell::new(None);
#[thread_local]
static LOCAL_BUFFER: RefCell<[u8; PAGE_SIZE]> = RefCell::new([0u8; PAGE_SIZE]);
#[thread_local]
static LOCAL_BUFFER_START: Cell<usize> = Cell::new(0);

static LOG_PROCESS_NAME: &'static str = "proc";

static DMESG_OUTPUT_FD: AtomicI32 = AtomicI32::new(0);

pub fn use_dev_dmesg_thread(enable: Option<bool>) {
	USE_DEV_DMESG_THREAD.set(enable);
}

struct DmesgOutHandle;

impl fmt::Write for DmesgOutHandle {
	fn write_str(&mut self, s: &str) -> fmt::Result {
		let start = LOCAL_BUFFER_START.get();
		let mut local_buffer = LOCAL_BUFFER.borrow_mut();

		let fd = get_fd(DMESG_OUTPUT_FD.load(Ordering::Relaxed));

		let out = if s.ends_with("\n") && start == 0 {
			s.as_bytes()
		}else{
			if start + s.len() > local_buffer.len(){
				let _ = fd.write(&local_buffer[..start]);
				s.as_bytes()
			}else{
				local_buffer[start..start + s.len()].copy_from_slice(s.as_bytes());
				let end = start + s.len();
				if !s.ends_with("\n") {
					LOCAL_BUFFER_START.set(end);
					return Ok(())
				}
				&local_buffer[..end]
			}
		};
		LOCAL_BUFFER_START.set(0);
		let _ = fd.write(&out);
		Ok(())
	}
}


struct Logger;

impl log::Log for Logger {
	fn enabled(&self, metadata: &Metadata) -> bool {
		metadata.level() <= Level::Debug
	}

	fn log(&self, record: &Record) {
		if self.enabled(record.metadata()) {
			let use_dmesg = if let Some(enabled) = USE_DEV_DMESG_THREAD.get() {
				enabled
			}else{
				USE_DEV_DMESG_DEFAULT.load(Ordering::Relaxed)
			};
			let mut dmesg_output_handle = DmesgOutHandle;
			let mut direct_output_handle = sel4::DebugOutHandle;
			let output_handle: &mut dyn fmt::Write = if use_dmesg {
				&mut dmesg_output_handle
			}else{
				&mut direct_output_handle
			};
			if let Some(name) = get_current_name() {
				let _ = writeln!(output_handle, "{} ({}:{}): {}", LOG_PROCESS_NAME, name, get_current_tid(), record.args());
			}else{
				let _ = writeln!(output_handle, "{} (<unset>): {}", LOG_PROCESS_NAME, record.args());
			}

		}
	}
	fn flush(&self) {}
}

static LOGGER: Logger = Logger;

///Initializes the logger
pub fn init() -> Result<(), SetLoggerError> {
	log::set_logger(&LOGGER)
		.map(|()| log::set_max_level(LevelFilter::Info))
}

pub fn init_dmesg() {
	let rpc = get_default_rpc_client();
	let fd = rpc.open("/dev/dmesg".as_bytes(), O_RDWR as u64).expect("logger::init_dmesg: could not open /dev/dmesg");
	DMESG_OUTPUT_FD.store(fd, Ordering::Relaxed);
	USE_DEV_DMESG_DEFAULT.store(true, Ordering::Relaxed);
}
/* vim: set softtabstop=8 tabstop=8 noexpandtab: */
